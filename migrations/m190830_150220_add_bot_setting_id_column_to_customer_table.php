<?php

use yii\db\Migration;

/**
 * Handles adding bot_setting_id to table `customer`.
 */
class m190830_150220_add_bot_setting_id_column_to_customer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customer', 'bot_setting_id', $this->integer()->after('bot_id')->comment('Настройка бота'));

        $this->createIndex(
            'idx-customer-bot_setting_id',
            'customer',
            'bot_setting_id'
        );

        $this->addForeignKey(
            'fk-customer-bot_setting_id',
            'customer',
            'bot_setting_id',
            'bot_setting',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-customer-bot_setting_id',
            'customer'
        );

        $this->dropIndex(
            'idx-customer-bot_setting_id',
            'customer'
        );

        $this->dropColumn('customer', 'bot_setting_id');
    }
}
