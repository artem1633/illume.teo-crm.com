<?php

use yii\db\Migration;

/**
 * Handles adding last_chat_history_id to table `customer`.
 */
class m191005_180432_add_last_chat_history_id_column_to_customer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customer', 'last_chat_history_id', $this->integer()->comment('Последнее сообщение'));

        $this->createIndex(
            'idx-customer-last_chat_history_id',
            'customer',
            'last_chat_history_id'
        );

        $this->addForeignKey(
            'fk-customer-last_chat_history_id',
            'customer',
            'last_chat_history_id',
            'chat_history',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-customer-last_chat_history_id',
            'customer'
        );

        $this->dropIndex(
            'idx-customer-last_chat_history_id',
            'customer'
        );

        $this->dropColumn('customer', 'last_chat_history_id');
    }
}
