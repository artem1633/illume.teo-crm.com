<?php

use yii\db\Migration;

/**
 * Handles adding attachment to table `chat_history`.
 */
class m191007_234851_add_attachment_column_to_chat_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('chat_history', 'attachment', $this->string()->after('text')->comment('Ссылка на вложение'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('chat_history', 'attachment');
    }
}
