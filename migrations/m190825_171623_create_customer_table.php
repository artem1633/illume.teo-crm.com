<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer`.
 */
class m190825_171623_create_customer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer', [
            'id' => $this->primaryKey(),
            'last_name' => $this->string()->comment('Фамилия'),
            'name' => $this->string()->comment('Имя'),
            'patronymic' => $this->string()->comment('Очество'),
            'email' => $this->string()->comment('Email'),
            'phone' => $this->string()->comment('Телефон'),
            'status_id' => $this->integer()->comment('Статус'),
            'unknown_reaction' => $this->boolean()->defaultValue(false)->comment('Неизвестная реакция'),
            'new_message' => $this->boolean()->defaultValue(false)->comment('Новое сообщение'),
            'last_message_id' => $this->string()->comment('Внешний ID последнего сообщения'),
            'account_id' => $this->string()->comment('ID в соц. сети'),
            'last_message_datetime' => $this->dateTime()->comment('Дата и время последнего сообшения'),
            'bot_id' => $this->integer()->comment('Бот'),
            'company_id' => $this->integer()->comment('Компания'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-customer-status_id',
            'customer',
            'status_id'
        );

        $this->addForeignKey(
            'fk-customer-status_id',
            'customer',
            'status_id',
            'customer_status',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-customer-bot_id',
            'customer',
            'bot_id'
        );

        $this->addForeignKey(
            'fk-customer-bot_id',
            'customer',
            'bot_id',
            'bot',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-customer-company_id',
            'customer',
            'company_id'
        );

        $this->addForeignKey(
            'fk-customer-company_id',
            'customer',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-customer-company_id',
            'customer'
        );

        $this->dropIndex(
            'idx-customer-company_id',
            'customer'
        );

        $this->dropForeignKey(
            'fk-customer-bot_id',
            'customer'
        );

        $this->dropIndex(
            'idx-customer-bot_id',
            'customer'
        );

        $this->dropForeignKey(
            'fk-customer-status_id',
            'customer'
        );

        $this->dropIndex(
            'idx-customer-status_id',
            'customer'
        );

        $this->dropTable('customer');
    }
}
