<?php

use yii\db\Migration;

/**
 * Handles adding send_in_crm to table `bot_scenario`.
 */
class m190927_194619_add_send_in_crm_column_to_bot_scenario_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('bot_scenario', 'send_in_crm', $this->boolean()->defaultValue(false)->comment('Отправлять в CRM'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('bot_scenario', 'send_in_crm');
    }
}
