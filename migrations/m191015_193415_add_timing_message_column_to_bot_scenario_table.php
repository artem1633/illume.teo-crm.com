<?php

use yii\db\Migration;

/**
 * Handles adding timing_message to table `bot_scenario`.
 */
class m191015_193415_add_timing_message_column_to_bot_scenario_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('bot_scenario', 'timing_message', $this->text()->comment('Отложенное сообщение'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('bot_scenario', 'timing_message');
    }
}
