<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bot_scenarios`.
 * Has foreign keys to the tables:
 *
 * - `bot_scenario_from_id`
 * - `bot_scenario_to_id`
 */
class m190827_165940_create_junction_table_bot_scenario_and_bot_scenario_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bot_scenarios', [
            'id' => $this->primaryKey(),
            'bot_scenario_from_id' => $this->integer(),
            'bot_scenario_to_id' => $this->integer(),
        ]);

        $this->createIndex(
            'idx-bot_scenarios-bot_scenario_from_id',
            'bot_scenarios',
            'bot_scenario_from_id'
        );

        $this->addForeignKey(
            'fk-bot_scenarios-bot_scenario_from_id',
            'bot_scenarios',
            'bot_scenario_from_id',
            'bot_scenario',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-bot_scenarios-bot_scenario_to_id',
            'bot_scenarios',
            'bot_scenario_to_id'
        );

        $this->addForeignKey(
            'fk-bot_scenarios-bot_scenario_to_id',
            'bot_scenarios',
            'bot_scenario_to_id',
            'bot_scenario',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-bot_scenarios-bot_scenario_from_id',
            'bot_scenarios'
        );

        $this->dropIndex(
            'idx-bot_scenarios-bot_scenario_from_id',
            'bot_scenarios'
        );

        $this->dropForeignKey(
            'fk-bot_scenarios-bot_scenario_to_id',
            'bot_scenarios'
        );

        $this->dropIndex(
            'idx-bot_scenarios-bot_scenario_to_id',
            'bot_scenarios'
        );

        $this->dropTable('bot_scenarios');
    }
}
