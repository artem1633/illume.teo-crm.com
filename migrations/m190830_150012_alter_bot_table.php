<?php

use yii\db\Migration;

/**
 * Class m190830_150012_alter_bot_table
 */
class m190830_150012_alter_bot_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('bot', 'token');
        $this->dropColumn('bot', 'additional_setting');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('bot', 'token', $this->string()->after('name')->comment('Токен'));
        $this->addColumn('bot', 'additional_setting', $this->string()->after('token')->comment('Доп. настройка'));
    }
}
