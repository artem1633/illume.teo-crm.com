<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m190825_160659_create_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'city' => $this->string()->comment('Город'),
            'address' => $this->string()->comment('Адрес'),
            'house' => $this->string()->comment('Дом'),
            'flat' => $this->string()->comment('Квартира/Офис'),
            'post_index' => $this->string()->comment('Почтовый индекс'),
            'subscription_id' => $this->integer()->comment('Подписка'),
            'is_super_company' => $this->boolean()->defaultValue(false)->comment('Является ли супер компанией'),
            'subscription_end_datetime' => $this->dateTime()->comment('Дата и время окончания подписки'),
            'access' => $this->boolean()->defaultValue(true)->comment('Доступ (вкл/выкл)'),
            'last_activity_datetime' => $this->dateTime()->comment('Дата и время последней активности'),
            'balance' => $this->float()->comment('Баланс'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-company-subscription_id',
            'company',
            'subscription_id'
        );

        $this->addForeignKey(
            'fk-company-subscription_id',
            'company',
            'subscription_id',
            'subscription',
            'id',
            'SET NULL'
        );

        $this->insert('company', [
            'name' => 'Супер компания',
            'is_super_company' => 1
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-company-subscription_id',
            'company'
        );

        $this->dropIndex(
            'idx-company-subscription_id',
            'company'
        );

        $this->dropTable('company');
    }
}
