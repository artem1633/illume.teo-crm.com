<?php

use yii\db\Migration;

/**
 * Class m191027_093618_add_telegram_token_and_telegram_url_settings
 */
class m191027_093618_add_telegram_token_and_telegram_url_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'telegram_token',
            'label' => 'Токен телеграма'
        ]);

        $this->insert('settings', [
            'key' => 'telegram_url',
            'label' => 'Ссылка на телеграм'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => ['telegram_token', 'telegram_url']]);
    }
}
