<?php

use yii\db\Migration;

/**
 * Handles the creation of table `chat_history`.
 */
class m190825_174039_create_chat_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('chat_history', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->comment('Покупатель'),
            'bot_id' => $this->integer()->comment('Бот'),
            'text' => $this->binary()->comment('Текст сообщения'),
            'external_answer_message_id' => $this->string()->comment('Внешний ID ответа сообщения'),
            'message_send_datetime' => $this->dateTime()->comment('Дата и время отправки сообщения'),
            'unknown_reaction' => $this->boolean()->comment('Неизвестная реакция'),
            'sender' => $this->boolean()->comment('Отправитель'),
        ]);

        $this->createIndex(
            'idx-chat_history-customer_id',
            'chat_history',
            'customer_id'
        );

        $this->addForeignKey(
            'fk-chat_history-customer_id',
            'chat_history',
            'customer_id',
            'customer',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-chat_history-bot_id',
            'chat_history',
            'bot_id'
        );

        $this->addForeignKey(
            'fk-chat_history-bot_id',
            'chat_history',
            'bot_id',
            'bot',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-chat_history-bot_id',
            'chat_history'
        );

        $this->dropIndex(
            'idx-chat_history-bot_id',
            'chat_history'
        );

        $this->dropForeignKey(
            'fk-chat_history-customer_id',
            'chat_history'
        );

        $this->dropIndex(
            'idx-chat_history-customer_id',
            'chat_history'
        );

        $this->dropTable('chat_history');
    }
}
