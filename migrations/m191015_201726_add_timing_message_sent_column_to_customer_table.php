<?php

use yii\db\Migration;

/**
 * Handles adding timing_message_sent to table `customer`.
 */
class m191015_201726_add_timing_message_sent_column_to_customer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customer', 'timing_message_sent', $this->boolean()->defaultValue(false)->comment('Отложенное сообщение отправлено'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('customer', 'timing_message_sent');
    }
}
