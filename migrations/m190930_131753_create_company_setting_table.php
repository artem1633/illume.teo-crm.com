<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_setting`.
 */
class m190930_131753_create_company_setting_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company_setting', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->comment('Компания'),
            'bitrix_user_identity' => $this->string()->comment('ID пользователя Битрикс24'),
            'bitrix_webhook' => $this->string()->comment('Вебхук Битрикс24')
        ]);

        $this->createIndex(
            'idx-company_setting-company_id',
            'company_setting',
            'company_id'
        );

        $this->addForeignKey(
            'fk-company_setting-company_id',
            'company_setting',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-company_setting-company_id',
            'company_setting'
        );

        $this->dropIndex(
            'idx-company_setting-company_id',
            'company_setting'
        );

        $this->dropTable('company_setting');
    }
}
