<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bot_scenario_variant`.
 */
class m190825_171357_create_bot_scenario_variant_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bot_scenario_variant', [
            'id' => $this->primaryKey(),
            'content' => $this->text()->comment('Ответ'),
            'bot_scenario_id' => $this->integer()->comment('Сценарий бота'),
        ]);

        $this->createIndex(
            'idx-bot_scenario_variant-bot_scenario_id',
            'bot_scenario_variant',
            'bot_scenario_id'
        );

        $this->addForeignKey(
            'fk-bot_scenario_variant-bot_scenario_id',
            'bot_scenario_variant',
            'bot_scenario_id',
            'bot_scenario',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-bot_scenario_variant-bot_scenario_id',
            'bot_scenario_variant'
        );

        $this->dropIndex(
            'idx-bot_scenario_variant-bot_scenario_id',
            'bot_scenario_variant'
        );

        $this->dropTable('bot_scenario_variant');
    }
}
