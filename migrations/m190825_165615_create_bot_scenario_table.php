<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bot_scenario`.
 */
class m190825_165615_create_bot_scenario_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bot_scenario', [
            'id' => $this->primaryKey(),
            'bot_id' => $this->integer()->comment('Бот'),
            'name' => $this->string()->comment('Наименование'),
            'description' => $this->text()->comment('Описание'),
            'is_start' => $this->boolean()->defaultValue(false)->comment('Стартовый ли сценарий'),
            'strict_search_mode' => $this->boolean()->comment('Строгий режим поиска'),
            'customer_status_id' => $this->integer()->comment('Статус'),
            'repeat' => $this->boolean()->comment('Повторить ли вопрос'),
            'any_user_answer' => $this->boolean()->comment('Любой ответ пользователя'),
            'no_user_answer' => $this->boolean()->comment('Нет ответа от пользователя'),
            'no_answer_time' => $this->integer()->comment('Время отсутствия ответа'),
            'bot_answer' => $this->binary()->comment('Ответ бота'),
            'attachment_link' => $this->string()->comment('Ссылка на приложение (на файл)'),
        ]);

        $this->createIndex(
            'idx-bot_scenario-bot_id',
            'bot_scenario',
            'bot_id'
        );

        $this->addForeignKey(
            'fk-bot_scenario-bot_id',
            'bot_scenario',
            'bot_id',
            'bot',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-bot_scenario-customer_status_id',
            'bot_scenario',
            'customer_status_id'
        );

        $this->addForeignKey(
            'fk-bot_scenario-customer_status_id',
            'bot_scenario',
            'customer_status_id',
            'customer_status',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-bot_scenario-bot_id',
            'bot_scenario'
        );

        $this->dropIndex(
            'idx-bot_scenario-bot_id',
            'bot_scenario'
        );

        $this->dropForeignKey(
            'fk-bot_scenario-customer_status_id',
            'bot_scenario'
        );

        $this->dropIndex(
            'idx-bot_scenario-customer_status_id',
            'bot_scenario'
        );

        $this->dropTable('bot_scenario');
    }
}
