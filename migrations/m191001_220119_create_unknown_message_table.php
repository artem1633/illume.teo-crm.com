<?php

use yii\db\Migration;

/**
 * Handles the creation of table `unknown_message`.
 */
class m191001_220119_create_unknown_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('unknown_message', [
            'id' => $this->primaryKey(),
            'content' => $this->binary()->comment('Содержание'),
            'bot_id' => $this->integer()->comment('Бот'),
            'company_id' => $this->integer()->comment('Компания'),
            'created_at' => $this->dateTime()
        ]);


        $this->createIndex(
            'idx-unknown_message-bot_id',
            'unknown_message',
            'bot_id'
        );

        $this->addForeignKey(
            'fk-unknown_message-bot_id',
            'unknown_message',
            'bot_id',
            'bot',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-unknown_message-company_id',
            'unknown_message',
            'company_id'
        );

        $this->addForeignKey(
            'fk-unknown_message-company_id',
            'unknown_message',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-unknown_message-company_id',
            'unknown_message'
        );

        $this->dropIndex(
            'idx-unknown_message-company_id',
            'unknown_message'
        );


        $this->dropForeignKey(
            'fk-unknown_message-bot_id',
            'unknown_message'
        );

        $this->dropIndex(
            'idx-unknown_message-bot_id',
            'unknown_message'
        );

        $this->dropTable('unknown_message');
    }
}
