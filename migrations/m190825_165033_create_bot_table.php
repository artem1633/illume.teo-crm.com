<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bot`.
 */
class m190825_165033_create_bot_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bot', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'token' => $this->string()->comment('Токен'),
            'additional_setting' => $this->string()->comment('Доп. настройка'),
            'type' => $this->string()->comment('Тип'),
            'status' => $this->integer()->comment('Статус'),
            'description' => $this->text()->comment('Текст'),
            'using_as_template' => $this->boolean()->comment('Используется ли как шаблон'),
            'company_id' => $this->integer()->comment('Компания'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-bot-company_id',
            'bot',
            'company_id'
        );

        $this->addForeignKey(
            'fk-bot-company_id',
            'bot',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-bot-company_id',
            'bot'
        );

        $this->dropIndex(
            'idx-bot-company_id',
            'bot'
        );

        $this->dropTable('bot');
    }
}
