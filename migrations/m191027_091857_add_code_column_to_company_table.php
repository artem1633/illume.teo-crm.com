<?php

use yii\db\Migration;

/**
 * Handles adding code to table `company`.
 */
class m191027_091857_add_code_column_to_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('company', 'code', $this->string()->comment('Код'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('company', 'code');
    }
}
