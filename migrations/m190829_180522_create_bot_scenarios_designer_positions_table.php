<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bot_scenarios_designer_positions`.
 */
class m190829_180522_create_bot_scenarios_designer_positions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bot_scenarios_designer_positions', [
            'id' => $this->primaryKey(),
            'bot_id' => $this->integer()->notNull()->comment('Бот'),
            'group_id' => $this->string()->comment('ID группы в HTML'),
            'offset_top' => $this->integer()->comment('Ось сверху'),
            'offset_left' => $this->integer()->comment('Ось слева'),
        ]);

        $this->createIndex(
            'idx-bot_scenarios_designer_positions-bot_id',
            'bot_scenarios_designer_positions',
            'bot_id'
        );

        $this->addForeignKey(
            'fk-bot_scenarios_designer_positions-bot_id',
            'bot_scenarios_designer_positions',
            'bot_id',
            'bot',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-bot_scenarios_designer_positions-bot_id',
            'bot_scenarios_designer_positions'
        );

        $this->dropIndex(
            'idx-bot_scenarios_designer_positions-bot_id',
            'bot_scenarios_designer_positions'
        );

        $this->dropTable('bot_scenarios_designer_positions');
    }
}
