<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bot_setting`.
 */
class m190830_145007_create_bot_setting_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bot_setting', [
            'id' => $this->primaryKey(),
            'bot_id' => $this->integer()->comment('Бот'),
            'name' => $this->string()->comment('Наименование'),
            'type' => $this->integer()->comment('Тип'),
            'link' => $this->string()->comment('Ссылка'),
            'status' => $this->integer()->comment('Статус'),
            'token' => $this->string()->comment('Токен'),
            'additional_setting' => $this->string()->comment('Дополнительная настройка'),
            'group_id' => $this->string()->comment('ID группы'),
            'time_code' => $this->string()->comment('Временный код'),
            'company_id' => $this->integer()->comment('Компания'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-bot_setting-bot_id',
            'bot_setting',
            'bot_id'
        );

        $this->addForeignKey(
            'fk-bot_setting-bot_id',
            'bot_setting',
            'bot_id',
            'bot',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-bot_setting-company_id',
            'bot_setting',
            'company_id'
        );

        $this->addForeignKey(
            'fk-bot_setting-company_id',
            'bot_setting',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-bot_setting-bot_id',
            'bot_setting'
        );

        $this->dropIndex(
            'idx-bot_setting-bot_id',
            'bot_setting'
        );

        $this->dropForeignKey(
            'fk-bot_setting-company_id',
            'bot_setting'
        );

        $this->dropIndex(
            'idx-bot_setting-company_id',
            'bot_setting'
        );

        $this->dropTable('bot_setting');
    }
}
