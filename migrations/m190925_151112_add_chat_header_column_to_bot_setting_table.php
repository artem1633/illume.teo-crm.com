<?php

use yii\db\Migration;

/**
 * Handles adding chat_header to table `bot_setting`.
 */
class m190925_151112_add_chat_header_column_to_bot_setting_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('bot_setting', 'chat_header', $this->string()->after('time_code')->comment('Заголовок чата'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('bot_setting', 'chat_header');
    }
}
