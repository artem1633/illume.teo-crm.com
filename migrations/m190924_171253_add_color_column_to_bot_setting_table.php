<?php

use yii\db\Migration;

/**
 * Handles adding color to table `bot_setting`.
 */
class m190924_171253_add_color_column_to_bot_setting_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('bot_setting', 'color', $this->string()->after('time_code')->comment('Цвет чата'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('bot_setting', 'color');
    }
}
