<?php

use yii\db\Migration;

/**
 * Class m190930_144711_alter_using_as_template_column_to_bot_table
 */
class m190930_144711_alter_using_as_template_column_to_bot_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('bot', 'using_as_template', $this->boolean()->defaultValue(false)->comment('Использовать как шаблон'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    }
}
