<?php

use yii\db\Migration;

/**
 * Handles the creation of table `report`.
 */
class m190927_164215_create_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('report', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->comment('Компания'),
            'customer_id' => $this->integer()->comment('Покупатель'),
            'bot_id' => $this->integer()->comment('Бот'),
            'bot_setting_id' => $this->integer()->comment('Настройки бота'),
            'type' => $this->integer()->comment('Тип'),
            'value' => $this->string()->comment('Значение'),
            'comment' => $this->text()->comment('Комментарий'),
            'created_at' => $this->dateTime()->comment('Дата и время'),
        ]);

        $this->createIndex(
            'idx-report-company_id',
            'report',
            'company_id'
        );

        $this->addForeignKey(
            'fk-report-company_id',
            'report',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-report-customer_id',
            'report',
            'customer_id'
        );

        $this->addForeignKey(
            'fk-report-customer_id',
            'report',
            'customer_id',
            'customer',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-report-bot_id',
            'report',
            'bot_id'
        );

        $this->addForeignKey(
            'fk-report-bot_id',
            'report',
            'bot_id',
            'bot',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-report-bot_setting_id',
            'report',
            'bot_setting_id'
        );

        $this->addForeignKey(
            'fk-report-bot_setting_id',
            'report',
            'bot_setting_id',
            'bot_setting',
            'id',
            'CASCADE'
        );


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-report-bot_setting_id',
            'report'
        );

        $this->dropIndex(
            'idx-report-bot_setting_id',
            'report'
        );

        $this->dropForeignKey(
            'fk-report-bot_id',
            'report'
        );

        $this->dropIndex(
            'idx-report-bot_id',
            'report'
        );

        $this->dropForeignKey(
            'fk-report-customer_id',
            'report'
        );

        $this->dropIndex(
            'idx-report-customer_id',
            'report'
        );

        $this->dropForeignKey(
            'fk-report-company_id',
            'report'
        );

        $this->dropIndex(
            'idx-report-company_id',
            'report'
        );

        $this->dropTable('report');
    }
}
