<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer_status`.
 */
class m190825_164532_create_customer_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'color' => $this->string()->comment('Цвет'),
            'company_id' => $this->integer()->comment('Компания'),
        ]);

        $this->createIndex(
            'idx-customer_status-company_id',
            'customer_status',
            'company_id'
        );

        $this->addForeignKey(
            'fk-customer_status-company_id',
            'customer_status',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-customer_status-company_id',
            'customer_status'
        );

        $this->dropIndex(
            'idx-customer_status-company_id',
            'customer_status'
        );

        $this->dropTable('customer_status');
    }
}
