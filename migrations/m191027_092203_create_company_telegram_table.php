<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_telegram`.
 */
class m191027_092203_create_company_telegram_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company_telegram', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->comment('Компания'),
            'telegram_id' => $this->string()->comment('Телеграм'),
        ]);

        $this->createIndex(
            'idx-company_telegram-company_id',
            'company_telegram',
            'company_id'
        );

        $this->addForeignKey(
            'fk-company_telegram-company_id',
            'company_telegram',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-company_telegram-company_id',
            'company_telegram'
        );

        $this->dropIndex(
            'idx-company_telegram-company_id',
            'company_telegram'
        );

        $this->dropTable('company_telegram');
    }
}
