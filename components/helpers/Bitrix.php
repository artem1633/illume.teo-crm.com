<?php

namespace app\components\helpers;

use app\models\CompanySetting;
use Yii;


/**
 * Class Bitrix
 * @package app\components\helpers
 */
class Bitrix
{
    public static function sendLid($fields)
    {
        /** Bot @var $bot */
        $setting = CompanySetting::find()->where(['company_id' =>$fields['company_id']])->one();

        $userIdentity = $setting->bitrix_user_identity;
        $webhook = $setting->bitrix_webhook;

        // $url = "https://illumeinc.bitrix24.ru/rest/{$userIdentity}/{$webhook}/crm.lead.add.json";
        $url = "https://b24-egq19l.bitrix24.ru/rest/{$userIdentity}/{$webhook}/crm.lead.add.json";

        $fields = http_build_query([
            'fields' => $fields,
            'params' => ['REGISTER_SONET_EVENT' => 'Y']
        ]);

        $ch = curl_init($url);

        // обращаемся к Битрикс24 при помощи функции curl_exec
        curl_setopt_array($ch, [
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POSTFIELDS => $fields,
        ]);
        $result = json_decode(curl_exec($ch), true);

        \Yii::warning($result, 'Response from illume to bitrix24');

        curl_close($ch);


        return $result;

    }
}