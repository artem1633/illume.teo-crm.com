<?php

namespace app\components\helpers;

use app\models\Bot;
use app\models\BotScenario;
use app\models\BotScenarios;
use app\models\BotScenarioVariant;
use app\models\BotSetting;
use app\models\Customer;
use app\models\UnknownMessage;
use app\models\CompanyTelegram;
use app\models\ChatHistory;
use app\modules\api\controllers\BotinfoController;
use app\modules\api\controllers\SiteController;
use app\modules\api\controllers\TelegramNotifyController;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class AnswerFinder
{
    /**
     * @param Customer $user
     * @param Bot $bot
     * @param BotSetting $botSetting
     * @param string $text
     * @return bool
     */
    public static function find($user, $bot, $botSetting, $text)
    {
        $text = mb_strtolower($text);
        $text = preg_replace('/\pP/iu', ' ', $text);
        $bodyArr = explode(' ', $text);

        \Yii::warning("\"{$text}\"", 'Text find');
        \Yii::warning(implode('; ', $bodyArr), 'Body array');

        $list = ArrayHelper::getColumn(BotScenarios::find()->where(['bot_scenario_from_id' => $user->last_message_id])->all(),'bot_scenario_to_id');
        /** @var Bot $botDialog */
        $scenarios = BotScenario::find()
            ->where(['bot_id' => $bot->id])
            ->andFilterWhere(['id' => $list])
            ->orderBy(['any_user_answer' => SORT_ASC])
            ->all();

        $status = false;
        $scenario = false;


        /** @var BotScenario $scenario */
        /** @var BotScenario $item */
        /** @var BotScenarioVariant $variant */
        foreach ($scenarios as $item) {
            if (!$item->any_user_answer) {
                if ($item->strict_search_mode) {

                    // echo 'Strict searching mode here<br>';
                    // echo $text;
                    // exit;

                    $variant = BotScenarioVariant::find()
                        ->where(['bot_scenario_id' => $item->id])
                        ->andWhere(['content' => $text])
                        ->one();
                    if ($variant) {
                        $status = true;
                        $scenario = $item;
                        break;
                    } else {
                        $status = false;
                        continue;
                    }
                } else {
                    $variant = BotScenarioVariant::find()
                        ->where(['bot_scenario_id' => $item->id])
                        ->all();
                    $variant2 = null;
                    foreach ($variant as $var) {
                        // $pos = in_array($var->content, $bodyArr);
                        $pos = false;

                        \Yii::warning(serialize($var->content));
                        \Yii::warning(serialize($bodyArr[0]));

                        foreach ($bodyArr as $ba) {
                            $var->content = mb_strtolower($var->content);
                            \Yii::warning('here');
                            if($ba === $var->content){
                                $pos = true;
                            }
                        }

                        if ($pos == true) {
                            $variant2 = $var;
                            break;
                        }
                    }
                    if ($variant2) {
                        $status = true;
                        $scenario = $item;
                        break;
                    } else {
                        $status = false;
                        continue;
                    }
                }

            } else {
                $scenario = $item;
                $status = true;
            }
        }

        \Yii::warning($scenario, 'scenario found');


        if (!$status) {
            // BotinfoController::sendTelMessage('247187885', "!status", $botSetting);
            $user->unknown_reaction = true;

            // Уведомление о неизвестной реакции
            $companyTelegramIds = ArrayHelper::getColumn(CompanyTelegram::find()->where(['company_id' => $user->company_id])->all(), 'telegram_id');
            if(count($companyTelegramIds) > 0){
                $url = Url::toRoute(['/customer/index', 'CustomerSearch[activeCustomerId]' => $user->id], true);
                $reactionText = "Неизвестная реакция в диалоге с покупателем «{$user->fio}»\n Подробнее по ссылке: {$url}";
                TelegramNotifyController::sendTelMessage($companyTelegramIds, $reactionText);
            }

            $unMessage = new UnknownMessage([
                'bot_id' => $bot->id,
                'company_id' => $user->company_id,
                'content' => $text
            ]);
            $unMessage->save(false);
        }
        if ($status) {
//            self::sendTelMessage('247187885', "status", $bot);
            if($scenario->send_in_crm == 1){
                Bitrix::sendLid([
                    'company_id' => $user->company_id,
                    'TITLE' => $user->fio,
                    'NAME' => $user->name,
                    'LAST_NAME' => $user->last_name,
                    'SECOND_NAME' => $user->patronymic,
                    'SOURCE_ID' => 'OTHER',
                    'COMMENTS' => "Пришел из «".BotSetting::getTypes()[$botSetting->type]."». Бот «{$botSetting->name}»"
                ]);
            }
            if ($scenario->customer_status_id) {
                $user->status_id = $scenario->customer_status_id;
            }
            $user->last_message_id = $scenario->id;
            $user->last_message_datetime = date('Y-m-d H:i:s');
        }
        $user->save(false);

        if (!$scenario || is_bool($scenario)) {

            \Yii::warning($user, 'user dump');

            $chatHistory = BotinfoController::newStep($user, $bot, $text,1,0);
            if($chatHistory){
                SiteController::sendAdminMessage($chatHistory, $text);
            }
            else {
                $chatHistory = new ChatHistory([
                    'customer_id' => $user->id,
                    'text' => $text,
                ]);
                SiteController::sendAdminMessage($chatHistory, $text);
            }
            // BotinfoController::sendTelMessage('247187885', 'Не нашли подходящий ответ', $botSetting);
            return true;
        }
        $chatHistory = BotinfoController::newStep($user, $bot,$text,0,0);
        if($user->botSetting->type != 2){
            $user->message->sendMessage($user->account_id, $scenario->bot_answer);
        }
        if($scenario->hasFile()){
            $user->message->sendFile('https://app.illumeinc.com/'.$scenario->attachment_link);
        }

        // BotinfoController::sendTelMessage($user->id, $scenario->bot_answer, $botSetting);
        
        $chatHistoryBot = BotinfoController::newStep($user, $bot,$scenario->bot_answer,0,1);

        if($chatHistory){
            SiteController::sendAdminMessage($chatHistory, $text);
        }
        SiteController::sendAdminMessage($chatHistoryBot, $scenario->bot_answer, true);
        if($scenario->hasFile()){
            SiteController::sendAdminAttachmentMessage($chatHistoryBot, $scenario->attachment_link, true);
        }


        return $scenario;
    }
}