<?php

namespace app\components\messagers;

use app\models\Settings;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class TelegramMessage
 * @package app\componets\messagers
 *
 */
class TelegramMessage extends IMessage
{
    /**
     * @inheritdoc
     */
    public function sendMessage($userId, $text, $options = null)
    {
        $token = $this->botSetting->token;
//        $proxy_server = '188.166.160.106:80';
        $proxy_server = Settings::findByKey('proxy_server')->value;

        $userId = $this->customer->account_id;

        if(is_array($userId)){
            foreach ($userId as $id) {
                $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
                $url=$url.'?'.http_build_query(['chat_id' => $id, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
               // curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
                // curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

                curl_setopt($curl, CURLOPT_PROXY, $proxy);

            }
        } else {
            $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
            $url=$url.'?'.http_build_query(['chat_id' => $userId, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            // curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
           // curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $curl_scraped_page = curl_exec($ch);
            curl_close($ch);
        }

        \Yii::warning(serialize($curl_scraped_page), 'Telegram message sending result');

        return true;
    }

    public function sendFile($path)
    {
        $token = $this->botSetting->token;
//        $proxy_server = '188.166.160.106:80';
        $proxy_server = Settings::findByKey('proxy_server')->value;

        $userId = $this->customer->account_id;

        $this->SendTelFile($path, $token, $userId);

//         if(is_array($userId)){
//             foreach ($userId as $id) {
//                 $url = 'https://api.telegram.org/bot'.$token.'/sendDocument';
//                 // $url=$url.'?'.http_build_query(['chat_id' => $id, 'document' => $path]);//к нему мы прибавляем парметры, в виде GET-параметров

//                 $ch = curl_init();
//                 curl_setopt($ch, CURLOPT_URL,$url);
//                 curl_setopt($ch, CURLOPT_POST, 1);
//                 curl_setopt($ch, CURLOPT_POSTFIELDS, ['chat_id' => $userId, 'document' => $path, 'parse_mode' => 'HTML']);
// //                curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
//                 //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
//                 curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
//                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//                 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//                 curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type:multipart/form-data"]);
//                 curl_setopt($ch, CURLOPT_HEADER, 1);
//                 $curl_scraped_page = curl_exec($ch);
//                 curl_close($ch);

//                 curl_setopt($curl, CURLOPT_PROXY, $proxy);

//                 var_dump($curl_scraped_page);

//             }
//         } else {
//             $url = 'https://api.telegram.org/bot'.$token.'/sendDocument';
//             // $url=$url.'?'.http_build_query(['chat_id' => $userId, 'document' => $path, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

//             $ch = curl_init();
//             curl_setopt($ch, CURLOPT_URL,$url);
//             curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
//             curl_setopt($ch, CURLOPT_POST, 1);
//             curl_setopt($ch, CURLOPT_POSTFIELDS, ['chat_id' => $userId, 'caption' => 'Вложение', 'document' => $path, 'parse_mode' => 'HTML']);
// //            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
//             curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
//             curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//             curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//             curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type:multipart/form-data"]);
//             curl_setopt($ch, CURLOPT_HEADER, 1);
//             $curl_scraped_page = curl_exec($ch);
//             curl_close($ch);

//             var_dump($curl_scraped_page);
//         }

//         \Yii::warning('file sending '.$path.' | request url: '.$url.' | '.serialize($curl_scraped_page));

        return true;
    }

    private function SendTelFile($file_url,$token,$chatID) {
        $ch = curl_init($file_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $html = curl_exec($ch);
        curl_close($ch);
        file_put_contents(basename($file_url), $html);
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL =>  'https://api.telegram.org/bot'.$token.'/sendDocument?caption=Вложение&chat_id='.$chatID,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
                'Content-Type: multipart/form-data'
            ],
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => [
                'document' => curl_file_create(basename($file_url), mime_content_type(basename($file_url)), basename($file_url))
            ]
        ]);
        $data = curl_exec($curl);
        curl_close($curl);
    }
}