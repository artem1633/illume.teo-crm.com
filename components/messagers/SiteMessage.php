<?php

namespace app\components\messagers;

use yii\base\Component;

/**
 * Class SiteMessage
 * @package app\componets\messagers
 *
 */
class SiteMessage extends IMessage
{
    /**
     * @inheritdoc
     */
    public function sendMessage($userId, $text, $options = null)
    {
        $ch = curl_init('https://app.illumeinc.ru:3000/message');

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "chatId={$this->customer->account_id}&text={$text}");
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close ($ch);
    }

    public function sendFile($path)
    {
        $ch = curl_init('https://app.illumeinc.ru:3000/message');

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "chatId={$this->customer->account_id}&attachment={$path}");
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);
    }

    public function checkConnect()
    {
        $ch = curl_init('https://app.illumeinc.ru:3000/message');

        $postData = [
            'chatId' => $this->customer->account_id,
            'likeBot' => 1,
            'attachment' => null,
            'test' => 1
        ];

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            http_build_query($postData));
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);
    }
}