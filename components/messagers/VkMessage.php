<?php

namespace app\components\messagers;

use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class VkMessage
 * @package app\componets\messagers
 *
 */
class VkMessage extends IMessage
{
    /**
     * @inheritdoc
     */
    public function sendMessage($userId, $text, $options = null)
    {
        $request_params = array(
            'message' => $text,
            'user_id' => $this->customer->account_id,
            'access_token' => $this->botSetting->token,
            'v' => '5.21'
        );

        $params = http_build_query($request_params);

        // $result = file_get_contents('https://api.vk.com/method/messages.send?'. $get_params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.vk.com/method/messages.send');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);


        \Yii::warning('VK message sent with message: «'.$text.'»');
        \Yii::warning($curl_scraped_page, 'VK post response from messages.send');

        return true;
    }

    public function sendFile($path)
    {
        $request_params = array(
            'peer_id' => $this->customer->account_id,
            'access_token' => $this->botSetting->token,
            'type' => 'doc',
            'v' => '5.21'
        );

        $get_params = http_build_query($request_params);

        $result = json_decode(file_get_contents('https://api.vk.com/method/docs.getMessagesUploadServer?'. $get_params), true);

        \Yii::warning(serialize($result));

        $uploadUrl = $result['response']['upload_url'];

        $ch = curl_init($path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $html = curl_exec($ch);
        curl_close($ch);
        file_put_contents(basename($path), $html);
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL =>  $uploadUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
                'Content-Type: multipart/form-data'
            ],
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => [
                'file' => curl_file_create(basename($path), mime_content_type(basename($path)), basename($path))
            ]
        ]);
        $result = json_decode(curl_exec($curl), true);
        curl_close($curl);

        $file = $result['file'];


        $request_params = array(
            'title' => 'Документ',
            'file' => $file,
            // 'user_id' => $this->customer->account_id,
            'access_token' => $this->botSetting->token,
            // 'attachment' => 'doc187319659_'.$file,
            'v' => '5.21'
        );

        $get_params = http_build_query($request_params);

        $result = json_decode(file_get_contents('https://api.vk.com/method/docs.save?'. $get_params), true);

        $fileId = $result['response'][0]['id'];
        $ownerId = $result['response'][0]['owner_id'];

        $request_params = array(
            'message' => 'Приложение',
            'user_id' => $this->customer->account_id,
            'access_token' => $this->botSetting->token,
            'attachment' => "doc{$ownerId}_{$fileId}",
            'v' => '5.0'
        );

        $get_params = http_build_query($request_params);

        $result = file_get_contents('https://api.vk.com/method/messages.send?'. $get_params);

        return $result;
    }
}