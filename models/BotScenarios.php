<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bot_scenarios".
 *
 * @property int $bot_scenario_from_id
 * @property int $bot_scenario_to_id
 *
 * @property BotScenario $botScenarioFrom
 * @property BotScenario $botScenarioTo
 */
class BotScenarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bot_scenarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bot_scenario_from_id', 'bot_scenario_to_id'], 'integer'],
            [['bot_scenario_from_id'], 'exist', 'skipOnError' => true, 'targetClass' => BotScenario::className(), 'targetAttribute' => ['bot_scenario_from_id' => 'id']],
            [['bot_scenario_to_id'], 'exist', 'skipOnError' => true, 'targetClass' => BotScenario::className(), 'targetAttribute' => ['bot_scenario_to_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bot_scenario_from_id' => 'Bot Scenario From ID',
            'bot_scenario_to_id' => 'Bot Scenario To ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBotScenarioFrom()
    {
        return $this->hasOne(BotScenario::className(), ['id' => 'bot_scenario_from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBotScenarioTo()
    {
        return $this->hasOne(BotScenario::className(), ['id' => 'bot_scenario_to_id']);
    }
}
