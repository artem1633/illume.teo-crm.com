<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BotScenario;

/**
 * BotScenarioSearch represents the model behind the search form about `app\models\BotScenario`.
 */
class BotScenarioSearch extends BotScenario
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bot_id', 'strict_search_mode', 'customer_status_id', 'repeat', 'any_user_answer', 'no_user_answer', 'no_answer_time'], 'integer'],
            [['name', 'description', 'external_message_id', 'bot_answer', 'attachment_link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BotScenario::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'bot_id' => $this->bot_id,
            'strict_search_mode' => $this->strict_search_mode,
            'customer_status_id' => $this->customer_status_id,
            'repeat' => $this->repeat,
            'any_user_answer' => $this->any_user_answer,
            'no_user_answer' => $this->no_user_answer,
            'no_answer_time' => $this->no_answer_time,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'external_message_id', $this->external_message_id])
            ->andFilterWhere(['like', 'bot_answer', $this->bot_answer])
            ->andFilterWhere(['like', 'attachment_link', $this->attachment_link]);

        return $dataProvider;
    }
}
