<?php

namespace app\models;

use app\base\CompanyActiveRecord;
use app\componets\messagers\IMessage;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use app\modules\api\controllers\TelegramNotifyController;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $last_name Фамилия
 * @property string $name Имя
 * @property string $patronymic Очество
 * @property string $email Email
 * @property string $phone Телефон
 * @property int $status_id Статус
 * @property int $unknown_reaction Неизвестная реакция
 * @property int $new_message Новое сообщение
 * @property string $last_message_id Внешний ID последнего сообщения
 * @property string $account_id ID в соц. сети
 * @property string $last_message_datetime Дата и время последнего сообшения
 * @property int $bot_id Бот
 * @property int $bot_setting_id Настройка бота
 * @property int $company_id Компания
 * @property string $created_at
 * @property int $last_chat_history_id Последнее сообщение
 * @property integer $timing_message_sent Отложенное сообщение отправлено
 *
 * @property IMessage $message
 * @property string $fio
 *
 * @property ChatHistory[] $chatHistories
 * @property Bot $bot
 * @property Company $company
 * @property CustomerStatus $status
 */
class Customer extends CompanyActiveRecord
{
    private $_message;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['last_name', 'name'], 'required'],
            [['status_id', 'unknown_reaction', 'new_message', 'bot_id', 'company_id'], 'integer'],
            [['last_message_datetime', 'created_at', 'timing_message_sent'], 'safe'],
            [['last_name', 'name', 'patronymic', 'email', 'phone', 'last_message_id', 'account_id'], 'string', 'max' => 255],
            [['bot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bot::className(), 'targetAttribute' => ['bot_id' => 'id']],
            [['bot_setting_id'], 'exist', 'skipOnError' => true, 'targetClass' => BotSetting::className(), 'targetAttribute' => ['bot_setting_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['last_chat_history_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChatHistory::className(), 'targetAttribute' => ['last_chat_history_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'last_name' => 'Фамилия',
            'name' => 'Имя',
            'patronymic' => 'Очество',
            'email' => 'Email',
            'phone' => 'Телефон',
            'status_id' => 'Статус',
            'last_chat_history_id' => 'Последнее сообщение',
            'unknown_reaction' => 'Неизвестная реакция',
            'new_message' => 'Новое сообщение',
            'last_message_id' => 'Внешний ID последнего сообщения',
            'account_id' => 'ID в соц. сети',
            'last_message_datetime' => 'Дата и время последнего сообшения',
            'bot_id' => 'Бот',
            'bot_setting_id' => 'Настройка бота',
            'company_id' => 'Компания',
            'created_at' => 'Дата и время создания',
            'timing_message_sent' => 'Отложенное сообщение отправлено',
        ];
    }

    public function getMessage()
    {
        /** @var BotSetting $botSetting */
        $botSetting = $this->botSetting;
        if($this->_message == null && $botSetting != null){
            if(isset(BotSetting::getTypesClasses()[$botSetting->type]) == false){
                return null;
            }
            $this->_message = Yii::createObject(ArrayHelper::merge(['class' => BotSetting::getTypesClasses()[$botSetting->type]], [
                'botSetting' => $botSetting,
                'customer' => $this,
            ]));
        }

        return $this->_message;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if(in_array('status_id', array_keys($changedAttributes))){
            if($changedAttributes['status_id'] != $this->status_id){
                $report = new Report([
                    'type' => Report::TYPE_STATUS_CHANGED,
                    'customer_id' => $this->id,
                    'company_id' => $this->company_id,
                    'value' => $this->status_id,
                    'bot_id' => $this->bot_id,
                    'bot_setting_id' => $this->bot_setting_id,
                ]);
                $report->save(false);
                $this->unknown_reaction = 0;
                $this->save(false);
            }
        }

        if(in_array('last_message_id', array_keys($changedAttributes))){
            if($changedAttributes['last_message_id'] != $this->last_message_id){
                $this->timing_message_sent = 0;
                $this->save(false);
            }
        }
    }

    /**
     * @return string
     */
    public function getFio()
    {
        return "{$this->last_name} {$this->name} {$this->patronymic}";
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatHistories()
    {
        return $this->hasMany(ChatHistory::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['id' => 'bot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBotSetting()
    {
        return $this->hasOne(BotSetting::className(), ['id' => 'bot_setting_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(CustomerStatus::className(), ['id' => 'status_id']);
    }
}
