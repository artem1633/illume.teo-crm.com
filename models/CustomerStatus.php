<?php

namespace app\models;

use app\base\CompanyActiveRecord;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "customer_status".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $color Цвет
 * @property int $company_id Компания
 *
 * @property BotScenario[] $botScenarios
 * @property Customer[] $customers
 * @property Company $company
 */
class CustomerStatus extends CompanyActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['name', 'color'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'color' => 'Цвет',
            'company_id' => 'Компания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBotScenarios()
    {
        return $this->hasMany(BotScenario::className(), ['customer_status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
