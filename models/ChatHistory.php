<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "chat_history".
 *
 * @property int $id
 * @property int $customer_id Покупатель
 * @property int $bot_id Бот
 * @property resource $text Текст сообщения
 * @property string $attachment Вложения
 * @property string $external_answer_message_id Внешний ID ответа сообщения
 * @property string $message_send_datetime Дата и время отправки сообщения
 * @property int $unknown_reaction Неизвестная реакция
 * @property int $sender Отправитель
 *
 * @property Bot $bot
 * @property Customer $customer
 */
class ChatHistory extends \yii\db\ActiveRecord
{
    const SENDER_CUSTOMER = 0;
    const SENDER_BOT = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat_history';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['message_send_datetime'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'bot_id', 'unknown_reaction', 'sender'], 'integer'],
            [['text'], 'string'],
            [['message_send_datetime'], 'safe'],
            [['external_answer_message_id', 'attachment'], 'string', 'max' => 255],
            [['bot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bot::className(), 'targetAttribute' => ['bot_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Покупатель',
            'bot_id' => 'Бот',
            'text' => 'Текст сообщения',
            'external_answer_message_id' => 'Внешний ID ответа сообщения',
            'message_send_datetime' => 'Дата и время отправки сообщения',
            'unknown_reaction' => 'Неизвестная реакция',
            'attachment' => 'Вложения',
            'sender' => 'Отправитель',
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

//        if($this->customer_id != null){
//            Customer::updateAll(['last_chat_history_id' => $this->id], ['id' => $this->customer_id]);
//        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastChatHistory()
    {
        return $this->hasOne(ChatHistory::className(), ['id' => 'last_chat_history_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['id' => 'bot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
}
