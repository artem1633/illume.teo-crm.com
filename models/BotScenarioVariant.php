<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bot_scenario_variant".
 *
 * @property int $id
 * @property string $content Ответ
 * @property int $bot_scenario_id Сценарий бота
 *
 * @property BotScenario $botScenario
 */
class BotScenarioVariant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bot_scenario_variant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['bot_scenario_id'], 'integer'],
            [['bot_scenario_id'], 'exist', 'skipOnError' => true, 'targetClass' => BotScenario::className(), 'targetAttribute' => ['bot_scenario_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Ответ',
            'bot_scenario_id' => 'Сценарий бота',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBotScenario()
    {
        return $this->hasOne(BotScenario::className(), ['id' => 'bot_scenario_id']);
    }
}
