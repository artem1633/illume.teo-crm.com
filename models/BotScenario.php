<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "bot_scenario".
 *
 * @property int $id
 * @property int $bot_id Бот
 * @property string $name Наименование
 * @property string $description Описание
 * @property int $is_start Стартовый ли сценарий
 * @property int $next_scenario_id Следующий id сценария
 * @property int $strict_search_mode Строгий режим поиска
 * @property int $customer_status_id Статус
 * @property int $repeat Повторить ли вопрос
 * @property int $any_user_answer Любой ответ пользователя
 * @property int $no_user_answer Нет ответа от пользователя
 * @property int $no_answer_time Время отсутствия ответа
 * @property string $bot_answer Ответ бота
 * @property string $attachment_link Ссылка на приложение (на файл)
 * @property boolean $send_in_crm Отправлять в CRM
 * @property string $timing_message
 *
 * @property Bot $bot
 * @property CustomerStatus $customerStatus
 * @property BotScenarioVariant[] $botScenarioVariants
 * @property BotScenario[] $lastScenarios
 * @property BotScenario[] $nextScenarios
 */
class BotScenario extends \yii\db\ActiveRecord
{
    public $variants;

    public $lastScenarioIds;
    public $nextScenarioIds;

    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bot_scenario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bot_id', 'strict_search_mode', 'customer_status_id', 'repeat', 'any_user_answer', 'no_user_answer', 'no_answer_time', 'is_start', 'send_in_crm'], 'integer'],
            [['description', 'bot_answer', 'timing_message'], 'string'],
            [['name', 'attachment_link'], 'string', 'max' => 255],
            [['bot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bot::className(), 'targetAttribute' => ['bot_id' => 'id']],
            [['customer_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerStatus::className(), 'targetAttribute' => ['customer_status_id' => 'id']],
            [['variants', 'lastScenarioIds', 'nextScenarioIds'], 'safe'],
            ['file', 'file', 'skipOnEmpty' => true, 'maxSize' => 2e+7,],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bot_id' => 'Бот',
            'name' => 'Наименование',
            'is_start' => 'Стартовый',
            'description' => 'Описание',
            'strict_search_mode' => 'Строгий режим поиска',
            'customer_status_id' => 'Статус',
            'repeat' => 'Повторить ли вопрос',
            'any_user_answer' => 'Любой ответ пользователя',
            'no_user_answer' => 'Отложенное сообщение',
            'no_answer_time' => 'Время задержки ответа (мин.)',
            'file' => 'Файл',
            'bot_answer' => 'Ответ бота',
            'nextScenarioIds' => 'Следующие сценарии',
            'attachment_link' => 'Ссылка на приложение (на файл)',
            'send_in_crm' => 'Отправлять в CRM',
            'variants' => 'Варианты ответов',
            'timing_message' => 'Отложенное сообщение'
        ];
    }

    /**
     * @param bool $recursive
     * @return BotScenario
     */
    public function copy(&$beingIds = [], $recursive = true)
    {
        $model = new self();
        $model->attributes = $this->attributes;
        $model->save(false);


        if($recursive)
        {
            $lastScenariosIds = ArrayHelper::getColumn($this->lastScenarios, 'id');
            $nextScenarios = $this->nextScenarios;

            foreach ($lastScenariosIds as $id)
            {
                if(in_array($id, $beingIds)){
                    continue;
                }
                $scenarios = new BotScenarios(['bot_scenario_from_id' => $id, 'bot_scenario_to_id' => $model->id]);
                $scenarios->save(false);

                $beingIds[] = $id;
            }

            foreach ($nextScenarios as $scenario)
            {
                $beingIds[] = $scenario->id;
                if(in_array($scenario->id, $beingIds)){
                    $newModel = $scenario->copy($beingIds,false);
                } else {
                    $newModel = $scenario->copy($beingIds);
                }

                $scenarios = new BotScenarios(['bot_scenario_from_id' => $model->id, 'bot_scenario_to_id' => $newModel->id]);
                $scenarios->save(false);
            }
        }

        return $model;
    }

    public function hasFile() {
        return $this->attachment_link != null;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(is_dir('uploads') == false){
            mkdir('uploads');
        }

        if($this->file != null){
            $name = Yii::$app->security->generateRandomString();

            $this->file->saveAs('uploads/'.$name.'.'.$this->file->extension);

            $this->attachment_link = 'uploads/'.$name.'.'.$this->file->extension;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->variants != null){
            BotScenarioVariant::deleteAll(['bot_scenario_id' => $this->id]);
            foreach ($this->variants as $variant){
                $model = new BotScenarioVariant([
                    'bot_scenario_id' => $this->id,
                    'content' => $variant
                ]);
                $model->save(false);
            }
        }

        if($this->lastScenarioIds != null){
            BotScenarios::deleteAll(['bot_scenario_from_id' => $this->id]);
            foreach ($this->lastScenarioIds as $id){
                $model = new BotScenarios([
                    'bot_scenario_from_id' => $id,
                    'bot_scenario_to_id' => $this->id
                ]);
                $model->save(false);
            }
        }

        if($this->nextScenarioIds != null){
            BotScenarios::deleteAll(['bot_scenario_from_id' => $this->id]);
            foreach ($this->nextScenarioIds as $id){
                $model = new BotScenarios([
                    'bot_scenario_from_id' => $this->id,
                    'bot_scenario_to_id' => $id
                ]);
                $model->save(false);
            }
        }
    }

    /**
     * Ищет сценарии на которые можно установить в качестве следующих
     * @return array
     */
    public function getLinkableScenarios()
    {
        $noIds = [];

        $noIds = ArrayHelper::merge($noIds, ArrayHelper::getColumn($this->lastScenarios, 'id'));

//        return self::find()->where(['not in', 'id', $noIds])->all();
        return self::find()->where(['bot_id' => $this->bot_id])->all();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['id' => 'bot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerStatus()
    {
        return $this->hasOne(CustomerStatus::className(), ['id' => 'customer_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBotScenarioVariants()
    {
        return $this->hasMany(BotScenarioVariant::className(), ['bot_scenario_id' => 'id']);
    }

    /**
     * @return BotScenario[]
     */
    public function getNextScenarios()
    {
        $nextPks = ArrayHelper::getColumn(BotScenarios::find()->where(['bot_scenario_from_id' => $this->id])->all(), 'bot_scenario_to_id');
        return BotScenario::find()->where(['id' => $nextPks])->all();
    }

    /**
     * @return BotScenario[]
     */
    public function getLastScenarios()
    {
        $lastPks = ArrayHelper::getColumn(BotScenarios::find()->where(['bot_scenario_to_id' => $this->id])->all(), 'bot_scenario_from_id');
        return BotScenario::find()->where(['id' => $lastPks])->all();
    }
}
