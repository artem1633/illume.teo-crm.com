<?php

namespace app\models\forms;

use app\models\ChatHistory;
use yii\base\Model;
use app\models\Customer;


/**
 * Class CustomerAllMessageForm
 * @package app\models\forms
 */
class CustomerAllMessageForm extends Model
{
    /**
     * @var string
     */
    public $message;

    /**
     * @var integer
     */
    public $botId;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['message', 'botId'], 'required']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'message' => 'Сообщение',
            'botId' => 'Бот'
        ];
    }

    /**
     *
     */
    public function send()
    {
        $customers = Customer::find()->where(['bot_id' => $this->botId])->all();

        foreach ($customers as $customer) {
            if($customer->message != null){
                $customer->message->sendMessage($customer->account_id, $this->message);

                $msg = new ChatHistory([
                    'text' => $this->message,
                    'customer_id' => $customer->id,
                    'bot_id' => $this->botId,
                    'sender' => ChatHistory::SENDER_BOT,
                ]);
                $msg->save(false);
            }
        }
    }
}