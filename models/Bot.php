<?php

namespace app\models;

use app\base\CompanyActiveRecord;
use app\modules\api\controllers\BotinfoController;
use Yii;

/**
 * This is the model class for table "bot".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $type Тип
 * @property int $status Статус
 * @property string $description Текст
 * @property int $using_as_template Используется ли как шаблон
 * @property int $company_id Компания
 * @property string $created_at
 *
 * @property Company $company
 * @property BotScenario[] $botScenarios
 * @property ChatHistory[] $chatHistories
 * @property BotSetting[] $botSettings
 * @property Customer[] $customers
 */
class Bot extends CompanyActiveRecord
{
    public $coping = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status', 'using_as_template', 'company_id'], 'integer'],
            [['description'], 'string'],
            [['created_at'], 'safe'],
            [['name', 'type'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'type' => 'Тип',
            'status' => 'Статус',
            'description' => 'Текст',
            'using_as_template' => 'Используется ли как шаблон',
            'company_id' => 'Компания',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert && $this->coping == false){
//            $webhook = BotinfoController::setWebhook($this->id);
//            if (!$webhook['ok']) {
//                $this->status = 0;
//            } else {
//                $this->status = 1;
//            }
            $this->save(false);
            $scenario = new BotScenario([
                'name' => 'Приветствие',
                'is_start' => true,
                'bot_id' => $this->id,
                'bot_answer' => 'Приветствую. Я ваш лучший друг-бот!',
                'any_user_answer' => true
            ]);
            $scenario->save(false);
        }

    }


    public function copy()
    {
        $model = new self();
        $model->coping = true;
        $model->attributes = $this->attributes;
        $model->using_as_template = false;
        $model->company_id = Yii::$app->user->identity->company_id;

        $model->save(false);

        $scenariosMap = [];

        foreach ($this->getBotScenarios()->all() as $scenario)
        {
            $copiedScenario = new BotScenario();
            $copiedScenario->attributes = $scenario->attributes;
            $copiedScenario->bot_id = $model->id;
            $copiedScenario->save();
            $variants = BotScenarioVariant::find()->where(['bot_scenario_id' => $scenario->id])->all();
            foreach ($variants as $variant) {
                $newVariant = new BotScenarioVariant([
                    'content' => $variant->content,
                    'bot_scenario_id' => $copiedScenario->id,
                ]);
                $newVariant->save(false);
            }
            $botScenariosPosition = BotScenariosDesignerPositions::find()->where(['group_id' => "{$scenario->id}Group"])->one();
            if($botScenariosPosition != null){
                $newPosition = new BotScenariosDesignerPositions();
                $newPosition->bot_id = $model->id;
                $newPosition->offset_left = $botScenariosPosition->offset_left;
                $newPosition->offset_top = $botScenariosPosition->offset_top;
                $newPosition->group_id = "{$copiedScenario->id}Group";
                $newPosition->save();
            }
            $scenariosMap[$scenario->id] = $copiedScenario->id;
        }

        foreach ($scenariosMap as $old => $new)
        {
            $oldScenario = BotScenarios::find()->where(['bot_scenario_from_id' => $old])->all();
            foreach ($oldScenario as $scenario)
            {
                $newScenario = new BotScenarios();
                $newScenario->bot_scenario_from_id = $new;
                $newScenario->bot_scenario_to_id = $scenariosMap[$scenario->bot_scenario_to_id];
                $newScenario->save(false);
            }
        }




        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBotSettings()
    {
        return $this->hasMany(BotSetting::className(), ['bot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBotScenarios()
    {
        return $this->hasMany(BotScenario::className(), ['bot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatHistories()
    {
        return $this->hasMany(ChatHistory::className(), ['bot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['bot_id' => 'id']);
    }
}
