<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Customer;

/**
 * CustomerSearch represents the model behind the search form about `app\models\Customer`.
 */
class CustomerSearch extends Customer
{
    /**
     * @var integer
     */
    public $activeCustomerId;

    public $universal;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_id', 'unknown_reaction', 'new_message', 'bot_id', 'company_id', 'activeCustomerId'], 'integer'],
            [['last_name', 'name', 'patronymic', 'email', 'phone', 'last_message_id', 'account_id', 'last_message_datetime', 'created_at', 'universal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'unknown_reaction' => SORT_DESC,
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('chatHistories');

        $query->andFilterWhere([
            'id' => $this->id,
            'status_id' => $this->status_id,
            'unknown_reaction' => $this->unknown_reaction,
            'new_message' => $this->new_message,
            'last_message_datetime' => $this->last_message_datetime,
            'customer.bot_id' => $this->bot_id,
            'company_id' => $this->company_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'last_name', $this->universal])
            ->orFilterWhere(['like', 'name', $this->universal])
            ->orFilterWhere(['like', 'patronymic', $this->universal])
            ->orFilterWhere(['like', 'chat_history.text', $this->universal])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'last_message_id', $this->last_message_id])
            ->andFilterWhere(['like', 'account_id', $this->account_id]);

        if(Yii::$app->user->identity->isSuperAdmin() == false)
        {
            $query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        }

        $dataProvider->pagination = false;

        // $query->ignoreCompany();

        // echo count($query->all());
        // exit;
        // echo $query->createCommand()->getRawSql();
        // exit;

        return $dataProvider;
    }
}
