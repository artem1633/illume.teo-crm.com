<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bot_scenarios_designer_positions".
 *
 * @property int $id
 * @property int $bot_id Бот
 * @property string $group_id ID группы в HTML
 * @property int $offset_top Ось сверху
 * @property int $offset_left Ось слева
 *
 * @property Bot $bot
 */
class BotScenariosDesignerPositions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bot_scenarios_designer_positions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bot_id'], 'required'],
            [['bot_id', 'offset_top', 'offset_left'], 'integer'],
            [['group_id'], 'string', 'max' => 255],
            [['bot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bot::className(), 'targetAttribute' => ['bot_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bot_id' => 'Бот',
            'group_id' => 'ID группы в HTML',
            'offset_top' => 'Ось сверху',
            'offset_left' => 'Ось слева',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['id' => 'bot_id']);
    }
}
