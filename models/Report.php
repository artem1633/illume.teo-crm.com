<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "report".
 *
 * @property int $id
 * @property int $company_id Компания
 * @property int $customer_id Покупатель
 * @property int $bot_id Бот
 * @property int $bot_setting_id Настройки бота
 * @property int $type Тип
 * @property string $value Значение
 * @property string $comment Комментарий
 * @property string $created_at Дата и время
 *
 * @property Bot $bot
 * @property BotSetting $botSetting
 * @property Company $company
 * @property Customer $customer
 */
class Report extends \yii\db\ActiveRecord
{
    const TYPE_STATUS_CHANGED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'customer_id', 'bot_id', 'bot_setting_id', 'type'], 'integer'],
            [['comment'], 'string'],
            [['created_at'], 'safe'],
            [['value'], 'string', 'max' => 255],
            [['bot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bot::className(), 'targetAttribute' => ['bot_id' => 'id']],
            [['bot_setting_id'], 'exist', 'skipOnError' => true, 'targetClass' => BotSetting::className(), 'targetAttribute' => ['bot_setting_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'customer_id' => 'Покупатель',
            'bot_id' => 'Бот',
            'bot_setting_id' => 'Настройки бота',
            'type' => 'Тип',
            'value' => 'Значение',
            'comment' => 'Комментарий',
            'created_at' => 'Дата и время',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['id' => 'bot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBotSetting()
    {
        return $this->hasOne(BotSetting::className(), ['id' => 'bot_setting_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
}
