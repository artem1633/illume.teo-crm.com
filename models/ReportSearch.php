<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Report;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * ReportSearch represents the model behind the search form about `app\models\Report`.
 */
class ReportSearch extends Report
{
    public $dates;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'customer_id', 'bot_id', 'bot_setting_id', 'type'], 'integer'],
            [['value', 'comment', 'created_at', 'dates'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function searchForChart($params)
    {
        $query = Report::find();

        $report = [];

        $statuses = CustomerStatus::find()->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->dates != null) {
            $dates = explode(' - ', $this->dates);
            $dateStart = $dates[0];
            $dateEnd = $dates[1];

            $period = new \DatePeriod(
                new \DateTime($dateStart),
                new \DateInterval('P1D'),
                new \DateTime($dateEnd)
            );

            $report['dates'] = [];

            foreach ($period as $key => $value) {
                $report['dates'][] = $value->format('Y-m-d');
            }

        } else {
            $year = date('Y');
            $month = date('m');

            $lastDayMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year) + 1;

            $dateStart = date('Y-m-1');
            $dateEnd = date('Y-m-'.$lastDayMonth);
            for ($i = 1; $i < $lastDayMonth; $i++){
                if(strlen(strval($i)) == 1){
                    $i = '0'.$i;
                }
                $a [] = date('Y-m-'.$i);
            }

            $report['dates'] = $a;
        }


        $query->andFilterWhere(['between', 'created_at', $dateStart, $dateEnd]);

        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'customer_id' => $this->customer_id,
            'bot_id' => $this->bot_id,
            'bot_setting_id' => $this->bot_setting_id,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        $counter = 0;
        foreach ($statuses as $status)
        {
            $report['data'][$counter] = [
                'name' => $status->name,
                'data' => [],
            ];
            $counter++;
        }

        foreach ($report['dates'] as $date)
        {
            $counter = 0;
            $date = "{$date} ";
            foreach ($statuses as $status){
                $report['data'][$counter]['data'][] = intval(Report::find()->where(['like', 'created_at', $date])->andWhere(['type' => 1, 'value' => $status->id])->count());
                $counter++;
            }
        }



//        VarDumper::dump($report, 10, true);
//        exit;

        return $report;
    }
}
