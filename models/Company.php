<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $city Город
 * @property string $address Адрес
 * @property string $house Дом
 * @property string $flat Квартира/Офис
 * @property string $post_index Почтовый индекс
 * @property int $subscription_id Подписка
 * @property string $subscription_end_datetime Дата и время окончания подписки
 * @property int $access Доступ (вкл/выкл)
 * @property string $last_activity_datetime Дата и время последней активности
 * @property double $balance Баланс
 * @property string $created_at
 * @property string $code
 *
 * @property string $fullAddress
 *
 * @property Bot[] $bots
 * @property Subscription $subscription
 * @property Customer[] $customers
 * @property CustomerStatus[] $customerStatuses
 * @property Position[] $positions
 * @property User[] $users
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name',], 'required'],
            [['subscription_id', 'access'], 'integer'],
            [['subscription_end_datetime', 'last_activity_datetime', 'created_at'], 'safe'],
            [['balance'], 'number'],
            [['name', 'city', 'address', 'house', 'flat', 'post_index', 'code'], 'string', 'max' => 255],
            [['subscription_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subscription::className(), 'targetAttribute' => ['subscription_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'city' => 'Город',
            'address' => 'Адрес',
            'house' => 'Дом',
            'flat' => 'Квартира/Офис',
            'post_index' => 'Почтовый индекс',
            'subscription_id' => 'Подписка',
            'fullAddress' => 'Адрес',
            'subscription_end_datetime' => 'Дата и время окончания подписки',
            'access' => 'Доступ (вкл/выкл)',
            'last_activity_datetime' => 'Дата и время последней активности',
            'balance' => 'Баланс',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->generateCodeRecursive();
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param integer $i
     */
    private function generateCodeRecursive($i = 1)
    {
        if($i > 20){
            return;
        }

        $code = Yii::$app->security->generateRandomString(5);
        $company = self::find()->where(['code' => $code])->one();
        if($company == null){
            $this->code = $code;
            return;
        } else {
            $i++;
            $this->generateCodeRecursive($i);
        }
    }

    /**
     * @inheritdoc
     */
    public function getFullAddress()
    {
        $output = '';

        if($this->city != null){
            $output .= $this->city.', ';
        }

        if($this->address != null){
            $output .= $this->address.', ';
        }

        if($this->house != null){
            $output .= $this->house.', ';
        }

        if($this->flat != null){
            $output .= $this->flat.', ';
        }

        return $output;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBots()
    {
        return $this->hasMany(Bot::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['id' => 'subscription_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerStatuses()
    {
        return $this->hasMany(CustomerStatus::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(Position::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['company_id' => 'id']);
    }
}
