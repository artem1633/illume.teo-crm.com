<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BotSetting;

/**
 * BotSettingSearch represents the model behind the search form about `app\models\BotSetting`.
 */
class BotSettingSearch extends BotSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bot_id', 'type', 'status', 'company_id'], 'integer'],
            [['name', 'link', 'token', 'additional_setting', 'group_id', 'time_code', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BotSetting::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'bot_id' => $this->bot_id,
            'type' => $this->type,
            'status' => $this->status,
            'company_id' => $this->company_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'additional_setting', $this->additional_setting])
            ->andFilterWhere(['like', 'group_id', $this->group_id])
            ->andFilterWhere(['like', 'time_code', $this->time_code]);

        return $dataProvider;
    }
}
