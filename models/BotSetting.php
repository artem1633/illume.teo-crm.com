<?php

namespace app\models;

use app\base\CompanyActiveRecord;
use app\components\messagers\SiteMessage;
use app\components\messagers\VkMessage;
use app\components\messagers\TelegramMessage;
use app\modules\api\controllers\BotinfoController;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bot_setting".
 *
 * @property int $id
 * @property int $bot_id Бот
 * @property string $name Наименование
 * @property int $type Тип
 * @property string $link Ссылка
 * @property int $status Статус
 * @property string $token Токен
 * @property string $additional_setting Дополнительная настройка
 * @property string $group_id ID группы
 * @property string $time_code Временный код
 * @property string $chat_header Заголовок чата
 * @property int $company_id Компания
 * @property string $color Цвет
 * @property string $created_at
 *
 * @property Bot $bot
 * @property Company $company
 * @property Customer[] $customers
 */
class BotSetting extends CompanyActiveRecord
{
    const TYPE_VK = 0;
    const TYPE_TELEGRAM = 1;
    const TYPE_SITE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bot_setting';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'bot_id'], 'required'],
            [['bot_id', 'type', 'status', 'company_id'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'link', 'token', 'additional_setting', 'group_id', 'time_code', 'color', 'chat_header'], 'string', 'max' => 255],
            [['bot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bot::className(), 'targetAttribute' => ['bot_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bot_id' => 'Бот',
            'name' => 'Наименование',
            'type' => 'Тип',
            'link' => 'Ссылка',
            'status' => 'Статус',
            'token' => 'Токен',
            'additional_setting' => 'Дополнительная настройка',
            'group_id' => 'ID группы',
            'time_code' => 'Временный код',
            'chat_header' => 'Заголовок чата',
            'company_id' => 'Компания',
            'created_at' => 'Дата и время создания',
            'color' => 'Цвет',
        ];
    }

    /**
     * @return array
     */
    public static function getTypesClasses()
    {
        return [
            self::TYPE_VK => VkMessage::className(),
            self::TYPE_SITE => SiteMessage::className(),
            self::TYPE_TELEGRAM => TelegramMessage::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert && $this->type == self::TYPE_TELEGRAM){
            $webhook = BotinfoController::setWebhook($this->id);
            if (!$webhook['ok']) {
                $this->status = 0;
            } else {
                $this->status = 1;
            }
            $this->save();
        } else if($insert && $this->type == self::TYPE_SITE){
            $this->status = 1;
            $this->save();
        }
    }

    /**
     * @inheritdoc
     */
    public static function getTypes(){
        return [
            self::TYPE_VK => 'ВК',
            self::TYPE_TELEGRAM => 'Телеграм',
            self::TYPE_SITE => 'Сайт',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['id' => 'bot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['bot_setting_id' => 'id']);
    }
}
