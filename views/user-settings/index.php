<?php

/**
 * @var \app\models\CompanySetting $model
 * @var \yii\web\View $this
 */

$this->title = 'Настройки'

?>


<?php $form = \yii\widgets\ActiveForm::begin() ?>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Настройки</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'bitrix_user_identity')->textInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'bitrix_webhook')->textInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= \yii\helpers\Html::button('Сохранить', ['class' => 'btn btn-success btn-block', 'type' => 'submit']) ?>
            </div>
        </div>
    </div>
</div>

<?php \yii\widgets\ActiveForm::end() ?>
