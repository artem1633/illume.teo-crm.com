<?php

/** @var $this yii\web\View */
/** @var $report array */

use skeeks\widget\highcharts\Highcharts;

$this->title = 'Отчет';

// \yii\helpers\VarDumper::dump($report, 10, true);
// exit;

if(isset($report['data']) == false){
    $report['data'] = null;
}


?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Статистика</h4>
                </div>
                <div class="panel-body">
                    <?php \yii\widgets\Pjax::begin(['id' => 'pjax-chart-container']) ?>
                    <?php $form = \yii\widgets\ActiveForm::begin(['id' => 'filter-form', 'method' => 'GET']) ?>

                    <div class="row">
                        <div class="col-md-2">
                            <?=$form->field($searchModel, 'dates')->widget(\kartik\daterange\DateRangePicker::className(), [
                                'options' => [
                                    'autocomplete' => 'off',
                                    'class' => 'form-control'
                                ],
                                'convertFormat'=>true,
                                'pluginOptions' => [
                                    'opens'=>'right',
                                    'locale' => [
                                        'cancelLabel' => 'Clear',
                                        'format' => 'Y-m-d',
                                    ],
                                ]
                            ])->label('Даты')?>
                        </div>
                    </div>

                    <?php \yii\widgets\ActiveForm::end() ?>

                    <?= Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'height' => 225,
                                'type' => 'line'

                            ],
                            'title' => false,//['text' => 'Всего отправлено сообщений'],
                            //'subtitle' => ['text' => 'Notice the difference between a 0 value and a null point'],
                            'plotOptions' => [
                                'column' => ['depth' => 25],
                                'line' => [
                                    'dataLabels' => [
                                        'enabled' => true
                                    ]
                                ],
                                'enableMouseTracking' => false,
                            ],
                            'xAxis' => [
                                'categories' => $report['dates'],
                                'labels' => [
                                    'skew3d' => true,
                                    'style' => ['fontSize' => '10px']
                                ]
                            ],
                            'yAxis' => [
                                'title' => ['text' => null]
                            ],
                            'series' => $report['data'],
                            'credits' => ['enabled' => false],
                            'legend' => ['enabled' => true],
                        ]
                    ]);
                    ?>
                    <?php \yii\widgets\Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>

<?php

$script = <<< JS
    $('#filter-form input').change(function(){
        $('#filter-form').submit();
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>