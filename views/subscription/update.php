<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subscription */
?>
<div class="subscription-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
