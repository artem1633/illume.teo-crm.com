<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Subscription */

?>
<div class="subscription-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
