<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Bot */

?>
<div class="bot-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
