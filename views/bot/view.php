<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bot */

\johnitvn\ajaxcrud\CrudAsset::register($this);
\app\assets\plugins\SnapSvgAsset::register($this);

$this->params['minified-menu'] = true;

$this->title = "Дизайнер бота «{$model->name}»";

?>

<style>
    rect.active {
        fill: #5acce0;
    }

    .sizer a {
        display: block;
        width: 35px;
        height: 35px;
        background: #2d353c;
        text-align: center;
        vertical-align: middle;
        color: #fff;
        padding-top: 8px;
    }

    .sizer a:hover {
        background: #323b42;
        text-decoration: none;
    }


    .sizer a:first-child {
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        border-bottom: 1px solid #5e6a75;
    }

    .sizer a:last-child {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }

    .transltater a {
 		display: block;
        width: 35px;
        height: 35px;
        background: #2d353c;
        text-align: center;
        vertical-align: middle;
        color: #fff;
        padding-top: 8px;
        position: absolute;
    }

    #translate-up {
    	bottom: 0;
    	border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        border-bottom: 1px solid #5e6a75;
    }

    #translate-right {
    	left: 35px;
    	border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        border-left: 1px solid #5e6a75;
    }

    #translate-left {
    	right: 0;
    	border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
        border-right: 1px solid #5e6a75;
    }

    path {
        transition: all .25s;
    }

    path:hover {
        stroke: #00acac;
    }

</style>

<?php \yii\widgets\Pjax::begin(['id' => 'scenarios-pjax-container', 'enablePushState' => false]) ?>
<?php
$positions = \app\models\BotScenariosDesignerPositions::find()->where(['bot_id' => $model->id])->asArray()->all();
$positions = array_combine( \yii\helpers\ArrayHelper::getColumn($positions, 'group_id'), $positions);
$scenarios = \app\models\BotScenario::find()->where(['bot_id' => $model->id])->all();
?>

<div class="content-wrapper" style="position: fixed; top: 0; left: 0; width: 100000px; height: 100000px; transform-origin: top left; background: linear-gradient(#9aa0a6,transparent 1px),linear-gradient(90deg,#9aa0a6,transparent 1px);
    background-size: 20px 20px;">

    <?php
    $scenariosPks = \yii\helpers\ArrayHelper::getColumn($scenarios, 'id');
    $uniqueFrom = array_unique(\yii\helpers\ArrayHelper::getColumn(\app\models\BotScenarios::find()->where(['bot_scenario_from_id' => $scenariosPks])->all(), 'bot_scenario_from_id'));

    if(count($uniqueFrom) == 0){
        $uniqueFrom[] = \app\models\BotScenario::find()->where(['bot_id' => $model->id])->one()->id;
    }

    ?>
    <?php $counter = 1; foreach ($uniqueFrom as $from): ?>
        <?php
        $botScenariosPks = array_unique(\yii\helpers\ArrayHelper::getColumn(\app\models\BotScenarios::find()->where(['bot_scenario_from_id' => $from])->all(), 'bot_scenario_to_id'));
        /** @var \app\models\BotScenario[] $botScenarios */
        $botScenarios = \app\models\BotScenario::findAll($botScenariosPks);


        $scenario = \app\models\BotScenario::findOne($from);

        if(isset($positions[$scenario->id.'Group'])){
            $top = $positions[$scenario->id.'Group']['offset_top'];
            $left = $positions[$scenario->id.'Group']['offset_left'];
        } else {
            $top = 50;
            $left = 150 * $counter;
        }

        $counter++;
        ?>
        <div class="panel panel-inverse panel-drag" style="position: absolute; width: 200px; top: <?=$top?>px; left: <?=$left?>px;" data-rect="<?=$scenario->id?>Group">
            <a class="panel-heading" href="<?=\yii\helpers\Url::toRoute(['bot-scenario/update', 'id' => $scenario->id])?>"  data-edit="<?=$scenario->id?>" style="display: inline-block; position: absolute; width: 100%; height: 23px; z-index: 99;">
                <h4 class="panel-title"></h4>
                <?php
                //                echo Html::a('<i class="fa fa-pencil"></i>', ['bot-scenario/update', 'id' => $scenario->id], [
                //                    'class' => 'btn btn-primary btn-xs',
                //                    'style' => 'position: absolute; top: 0; right: 22px; z-index: 99;',
                //                    'role' => 'modal-remote'
                //                ])
                ?>
                <?= Html::a('<i class="fa fa-plus"></i>', ['bot-scenario/create', 'botId' => $model->id, 'id' => $scenario->id], [
                    'class' => 'btn btn-success btn-xs',
                    'style' => 'position: absolute; top: 0; right: 0; z-index: 99;',
                    'data-role' => 'create-scenario'
                ]) ?>
            </a>
            <div class="panel-body" style="padding-top: 35px;">
                <?=$scenario->name?>
                <div>
                    <?php foreach ($botScenarios as $botScenario): ?>
                        <div class="panel panel-inverse" data-rect="<?=$botScenario->id?>f<?=$from?>Inside" style="margin-bottom: 5px; position: relative; z-index: 99;">
                            <?php $relation = \app\models\BotScenarios::find()->where(['bot_scenario_from_id' => $botScenario->id])->one(); ?>
                            <a class="panel-heading" href="<?=\yii\helpers\Url::toRoute(['bot-scenario/update', 'id' => $botScenario->id])?>" data-has-relation="<?=$relation != null ? 'yes' : 'no'?>" data-edit="<?=$botScenario->id?>" style="display: block; position: relative; padding: 13px 15px; min-width: 100px;">
                                <h4 class="panel-title" style="width: 95%;"><?php
                                    $scenarioName = '';

                                    if(strlen($botScenario->name) > 18){
                                        $scenarioName = iconv_substr($botScenario->name, 0 , 18 , "UTF-8" ).'...';
                                    } else {
                                        $scenarioName = $botScenario->name;
                                    }

                                    echo $scenarioName;
                                    ?></h4>
                                <?php
                                //                                echo Html::a('<i class="fa fa-pencil"></i>', ['bot-scenario/update', 'id' => $botScenario->id], [
                                //                                    'class' => 'btn btn-primary btn-xs',
                                //                                    'style' => 'position: absolute; top: 24px; right: 0; z-index: 99;',
                                //                                    'role' => 'modal-remote'
                                //                                ])
                                ?>
                                <?php if($relation == null): ?>
                                    <?= Html::a('<i class="fa fa-plus"></i>', ['bot-scenario/create', 'botId' => $model->id, 'id' => $botScenario->id], [
                                        'class' => 'btn btn-success btn-xs',
                                        'style' => 'position: absolute; top: 12px; right: -9px; z-index: 99;',
                                        'data-role' => 'create-scenario'
                                    ]) ?>
                                <?php endif; ?>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endforeach;?>
    <svg id="svg" style="width: 100%; height: 3000px; position: fixed; top: 0; left 0;"></svg>
</div>

<div class="transltater" style="position: fixed; bottom: 104px; right: 220px;">
	<a id="translate-left" href="#" data-role="translate-left"><i class="fa fa-chevron-left"></i></a>
	<a id="translate-right" href="#" data-role="translate-right"><i class="fa fa-chevron-right"></i></a>
	<a id="translate-up" href="#" data-role="translate-up"><i class="fa fa-chevron-up"></i></a>
	<a id="translate-down" href="#" data-role="translate-down"><i class="fa fa-chevron-down"></i></a>
</div>

<div class="sizer" style="position: fixed; bottom: 70px; right: 70px;">
    <a href="#" data-role="size-plus"><i class="fa fa-plus"></i></a>
    <a href="#" data-role="size-minus"><i class="fa fa-minus"></i></a>
</div>

<?php

$rects = '';

$counter = 1;
//foreach ($scenarios as $scenario){
//
//    $top = 50;
//    $left = 150 * $counter;
//    $rects .= 'var rect'.$scenario->id.'Group = paper.draggableRect('.$left.','.$top.', 10,10, '.$scenario->id.');';
//
//    $counter++;
//}

foreach ($uniqueFrom as $from){
    $botScenariosPks = array_unique(\yii\helpers\ArrayHelper::getColumn(\app\models\BotScenarios::find()->where(['bot_scenario_from_id' => $from])->all(), 'bot_scenario_to_id'));
    /** @var \app\models\BotScenario[] $botScenarios */
    $botScenarios = \app\models\BotScenario::findAll($botScenariosPks);
    $scenario = \app\models\BotScenario::findOne($from);

    if(isset($positions[$scenario->id.'Group'])){
        $top = $positions[$scenario->id.'Group']['offset_top'];
        $left = $positions[$scenario->id.'Group']['offset_left'];
    } else {
        $top = 50;
        $left = 150 * $counter;
    }

    $rects .= 'var rect'.$scenario->id.'Group = paper.draggableRect('.$left.','.$top.', 10,10, '.$scenario->id.', "Group");';

    foreach($botScenarios as $botScenario){
        $insideTop = '$("[data-rect=\"'.$botScenario->id.'f'.$from.'Inside\"]").offset().top + 18';
        $insideLeft = '$("[data-rect=\"'.$botScenario->id.'f'.$from.'Inside\"]").offset().left + $("[data-rect=\"'.$botScenario->id.'f'.$from.'Inside\"]").outerWidth() - 11';
        $rects .= 'var rect'.$botScenario->id.'f'.$from.'Inside = paper.draggableRect('.$insideLeft.','.$insideTop.', 10,10, '.$botScenario->id.', "Inside", '.$from.');';
    }

    $counter++;
}


$paths = '';

$counter = 1;
//foreach ($scenarios as $scenario){
//
//    /** @var $scenario \app\models\BotScenario*/
//    foreach ($scenario->nextScenarios as $nextScenario){
//        $paths .= 'rect'.$scenario->id.'Group.addPath(rect'.$nextScenario->id.'Group);';
//    }
//}
foreach ($uniqueFrom as $from){
    $botScenariosPks = array_unique(\yii\helpers\ArrayHelper::getColumn(\app\models\BotScenarios::find()->where(['bot_scenario_from_id' => $from])->all(), 'bot_scenario_to_id'));
    /** @var \app\models\BotScenario[] $botScenarios */
    $botScenarios = \app\models\BotScenario::findAll($botScenariosPks);
    $top = 50;
    $left = 150 * $counter;
    $scenario = \app\models\BotScenario::findOne($from);



    foreach ($botScenarios as $botScenario){
        $paths .= 'if(typeof rect'.$botScenario->id.'f'.$from.'Inside != "undefined" && typeof rect'.$botScenario->id.'Group != "undefined"){
        rect'.$botScenario->id.'f'.$from.'Inside.addPath(rect'.$botScenario->id.'Group);
    }';

    }

    $counter++;
}

//$paths = 'if(typeof rect153Inside != "undefined" && typeof rect154Group != "undefined"){rect153Inside
//        .addPath(rect154Group);}if(typeof rect153Inside != "undefined" && typeof rect155Group != "undefined"){rect153Inside
//        .addPath(rect155Group);}';


function renderScenario($scenario, &$beingIds, $recurse = true){
    $panelClass = 'inverse';
    if($scenario->is_start) {
        $panelClass = 'success';
    }

    $beingIds[] = $scenario->id;

    $panelButtons = Html::a('<i class="fa fa-pencil"></i>', ['bot-scenario/update', 'id' => $scenario->id], ['class' => 'btn btn-info btn-xs', 'role' => 'modal-remote'])
        .Html::a('<i class="fa fa-plus"></i>', ['bot-scenario/create', 'id' => $scenario->id, 'botId' => $scenario->bot_id], ['class' => 'btn btn-success btn-xs', 'role' => 'modal-remote']);

    if($scenario->is_start == false){
        $panelButtons .= Html::a('<i class="fa fa-copy"></i>', ['bot-scenario/copy', 'id' => $scenario->id], ['class' => 'btn btn-primary btn-xs', 'role' => 'modal-remote', 'data-request-method'=>'post',
                'data-confirm-title'=>'Вы уверены?',
                'data-confirm-message'=>'Вы действительно хотите скопировать данный сценарий?'
            ]).Html::a('<i class="fa fa-trash"></i>', ['bot-scenario/delete', 'id' => $scenario->id], ['class' => 'btn btn-danger btn-xs', 'role' => 'modal-remote',
                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                'data-request-method'=>'post',
                'data-confirm-title'=>'Вы уверены?',
                'data-confirm-message'=>'Вы действительно хотите удалить данный сценарий?']);
    }

    $html = '
    <div class="panel panel-'.$panelClass.'" style="display: block; width: 100%;">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">'.$scenario->name.'</h4>
                                        <div class="panel-heading-btn" style="margin-top: -21px;">
                                            '.$panelButtons.'
                                        </div>
                                    </div>
                                    <div class="panel-body" style="white-space: initial; background: #cecece; border: 1px solid #000">
                                    '.$scenario->bot_answer.'
                                        ';

    if($recurse){
        foreach ($scenario->nextScenarios as $nextScenario){
            if(in_array($nextScenario->id, $beingIds)){
                $html .= renderScenario($nextScenario, $beingIds, false);
            } else {
                $html .= renderScenario($nextScenario, $beingIds);
            }
        }
    }

    $html .= '
                                    </div>
                                </div>
    ';

    return $html;
}

$script = <<< JS

// Работа с фомрмами создания/редактирования
var Form = function(){
    
}

isInitDraggable = false;

Form.edit = function(id){
    $.get('/bot-scenario/update?id='+id, function(response){
        $('.theme-panel').addClass('active');
        $('.theme-panel-content-title').html(response.title);
        $('.theme-panel-content-body').html(response.content);
    });
}

$('[data-role="create-scenario"]').click(function(e){
    e.preventDefault();
    
    $.get($(this).attr('href'), function(response){
        $('.theme-panel').addClass('active');
        $('.theme-panel-content-title').html(response.title);
        $('.theme-panel-content-body').html(response.content); 
    });
});

$('[data-edit]').click(function(e){
    e.preventDefault();
});

$('[data-edit]').dblclick(function(e){
    e.preventDefault();
    
    var id = $(this).data('edit');
    
    Form.edit(id);
});


var activeDragable = null;

var rects = [];

Snap.plugin(function (Snap, Element, Paper, global, Fragment) {
    function dragStart(x, y, e) {
        this.current_transform = this.transform();
    }

    function dragMove(dx, dy, x, y, e) {
        this.transform(this.current_transform+'T'+dx+','+dy);
        this.updatePaths();
    }

    function dragEnd(e) {
        this.current_transform = this.transform();
    }

    function updatePaths() {
        var key;
        for(key in this.paths) {
            this.paths[key][0].attr({"path" : this.getPathString(this.paths[key][1])});
            this.paths[key][0].prependTo(this.paper);
        }
    }

    function getCoordinates() {
        console.log(this.matrix);
        // return [this.offsetX, this.offsetY];
        return [this.matrix.e + (this.node.width.baseVal.value / 2),
          this.matrix.f + (this.node.height.baseVal.value / 2)];
    }
          
    function getPathString(obj) {
        var p1 = this.getCoordinates();
        var p2 = obj.getCoordinates();
        return "M"+p1[0]+","+p1[1]+"L"+p2[0]+","+p2[1];
    }

    function addPath(obj) {
        var id = obj.id;
        var path = this.paper.path(this.getPathString(obj)).attr({fill:'none', stroke:'#2d353c', strokeWidth:4});
        path.prependTo(this.paper);
        this.paths[id] = [path, obj];
        obj.paths[this.id] = [path, this];            
    }
    
    function removePath(obj) {
    		var id = obj.id;
        if (this.paths[id] != null) {
        		this.paths[id][0].remove();
            this.paths[id][1] = null;
            delete this.paths[id];
            
            obj.paths[this.id][1] = null;
            delete obj.paths[this.id];
        }
    }

    Paper.prototype.draggableRect = function (x, y, w, h, id, additionalId, from) {
        var rect = this.rect(0,0,w,h).transform("T"+x+","+y);
        rect.paths = {};
        // rect.drag(dragMove, dragStart, dragEnd);
        rect.updatePaths = updatePaths;
        rect.getCoordinates = getCoordinates;
        rect.getPathString = getPathString;
        rect.addPath = addPath;
        rect.removePath = removePath;
        if(from == undefined){
            rect.tagId = id+additionalId;       
        } else {
            rect.tagId = id+'f'+from+additionalId;       
        }
        
        rect.attr({'id': rect.tagId, fill: 'transparent'});
        
        rect.click(function(e){
            $('rect.active').removeClass('active');
            rect.addClass('active');
            activeDragable = rect;
        });
        
        rects.push(rect);

        return rect;
    };
});



var paper = Snap("#svg");

paper.click(function(e){
    if(e.target.tagName == 'svg'){
        if(activeDragable != null){
            var rect = paper.draggableRect(e.offsetX, e.offsetY, 40, 40);
        }
    }
});

var click = {
    x: 0,
    y: 0
};

function initDraggable(){
	if(isInitDraggable){
		$('.panel-drag').draggable("destroy");
		console.log('draggable destroyed');
	}
	$('.panel-drag').draggable({
		start: function(){
        	click.x = event.clientX;
        	click.y = event.clientY;
		},
	    drag: function(event, ui){
	        var rect = $(this).data('rect');
	        var rectObj = $('rect#'+rect);

			var original = ui.originalPosition;

	        ui.position = {
	            left: (event.clientX - click.x + original.left) / scale,
	            top:  (event.clientY - click.y + original.top ) / scale
	        };

	        // ui.position.left = event.offsetX;
	        // ui.position.top = event.offsetY;
	        // ui.position.left = Math.min( 100, ui.position.left );
	        rectObj.attr('transform', 'matrix(1,0,0,1,'+ui.position.left+','+ui.position.top+')');
	        console.log([ui.position.left, ui.position.top], 'drag position');
	        for(var i = 0; i < rects.length; i++){
	            var rectObj = rects[i];
	            if(rectObj.tagId == rect){
	                rectObj.transform('T'+ui.position.left+', '+ui.position.top+'');
	                rectObj.updatePaths();
	                break;
	            }
	        }
	        $(this).find('.panel').each(function(){
	            var rect = $(this).data('rect');
	            var rectObj = $('rect#'+rect);
				
				var positionLeft = ($(this).offset().left - click.x + original.left) / scale;
				var positionTop = ($(this).offset().top - click.y + original.top) / scale;

	            // rectObj.attr('transform', 'matrix(1,0,0,1,'+($(this).offset().left + $(this).outerWidth() - 11)+','+($(this).offset().top + 18)+')');
	            rectObj.attr('transform', 'matrix(1,0,0,1,'+(positionLeft + $(this).outerWidth() - 11)+','+(positionTop + 18)+')');
	            for(var i = 0; i < rects.length; i++){
	                var rectObj = rects[i];
	                if(rectObj.tagId == rect){
	                    // rectObj.transform('T'+($(this).offset().left + $(this).outerWidth() - 11)+', '+($(this).offset().top + 18)+'');
	                    rectObj.transform('T'+(positionLeft + $(this).outerWidth() - 11)+', '+(positionTop + 18)+'');
	                    rectObj.updatePaths();
	                    break;
	                }
	            }
	        });
	    },
	    stop: function(event, ui){
	        var csrfParam = yii.getCsrfParam();
	        var csrfToken = yii.getCsrfToken();

			var original = ui.originalPosition;

	      	ui.position = {
	            left: (event.clientX - click.x + original.left) / scale,
	            top:  (event.clientY - click.y + original.top ) / scale
	        };

	        $.ajax({
	            method: "POST",
	            url: 'change-coords',
	            data: {
	                csrfParam: csrfToken,
	                bot_id: '{$model->id}',
	                group_id: $(this).data('rect'),
	                offset_top: ui.position.top,
	                offset_left: ui.position.left
	            },
	        });
	        $(this).find('.panel').each(function(){
	            var rect = $(this).data('rect');
	            var rectObj = $('rect#'+rect);
				
				var positionLeft = ($(this).offset().left - click.x + original.left) / scale;
				var positionTop = ($(this).offset().top - click.y + original.top) / scale;

	            rectObj.attr('transform', 'matrix(1,0,0,1,'+(positionLeft + $(this).outerWidth() - 11)+','+(positionTop + 18)+')');
	            for(var i = 0; i < rects.length; i++){
	                var rectObj = rects[i];
	                if(rectObj.tagId == rect){
	                    rectObj.transform('T'+(positionLeft + $(this).outerWidth() - 11)+', '+(positionTop + 18)+'');
	                    rectObj.updatePaths();
	                    break;
	                }
	            }
	        });
	    },
	});
	isInitDraggable = true;
};

initDraggable();


function getRectById(id){
    for(var i = 0; i < rects.length; i++)
        {
            if(rects[i].tagId == id){
                return rects[i];
            }
        }
}

var scale = 1;
var translateTop = 0;
var translateLeft = 0;

$('[data-role="size-plus"]').click(function(e) {
    e.preventDefault();
    
    scale = scale + 0.1;
    
    $(".content-wrapper").css('transform', 'scale('+scale+')');
});

$('[data-role="size-minus"]').click(function(e) {
    e.preventDefault();
    
    scale = scale - 0.1;
    
    $(".content-wrapper").css('transform', 'scale('+scale+') translate('+translateLeft+'px, '+translateTop+'px)');
});

$('[data-role="translate-right"]').click(function(e){
	e.preventDefault();

	translateLeft = translateLeft - 100;

	$(".content-wrapper").css('transform', 'scale('+scale+') translate('+translateLeft+'px, '+translateTop+'px)');

});

$('[data-role="translate-left"]').click(function(e){
	e.preventDefault();

	if((translateLeft + 100) <= 0){
		translateLeft = translateLeft + 100;

		$(".content-wrapper").css('transform', 'scale('+scale+') translate('+translateLeft+'px, '+translateTop+'px)');
	}
});

$('[data-role="translate-up"]').click(function(e){
	e.preventDefault();

	if((translateTop + 100) <= 0){
		translateTop = translateTop + 100;

		$(".content-wrapper").css('transform', 'scale('+scale+') translate('+translateLeft+'px, '+translateTop+'px)');
	}
});

$('[data-role="translate-down"]').click(function(e){
	e.preventDefault();

	if((translateTop - 100) <= 0){
		translateTop = translateTop - 100;

		$(".content-wrapper").css('transform', 'scale('+scale+') translate('+translateLeft+'px, '+translateTop+'px)');
	}
});

// $('[data-role="creator"]').click(function(){
//     var currentRect = $(this).parent().parent().data('rect');
//     var newPanel = $(this).parent().parent().clone();
//     currentRect = getRectById(currentRect);
//     var rect = paper.draggableRect(300, 400, 40, 40, 3);
//     currentRect.addPath(rect);
//     newPanel.css('top', 400);    
//     newPanel.css('left', 300);
//     newPanel.draggable({
//         drag: function(event, ui){
//             var rect = $(this).data('rect');
//             var rectObj = $('rect#'+rect);
//             rectObj.attr('transform', 'matrix(1,0,0,1,'+ui.position.left+','+ui.position.top+')');
//             for(var i = 0; i < rects.length; i++){
//                 var rectObj = rects[i];
//                 if(rectObj.tagId == rect){
//                     rectObj.transform('T'+ui.position.left+', '+ui.position.top+'');
//                     rectObj.updatePaths();
//                     break;
//                 }
//             }
//         },
//     });
//     newPanel.data('rect', '3');
//     $('.content-wrapper').append(newPanel);
// });

// function onMouseDown(e) {

//     if (e.which != 1) return;

//     var elem = e.target.closest('.draggable');
//     if (!elem) return;

//     dragObject.elem = elem;


//     dragObject.downX = e.pageX;
//     dragObject.downY = e.pageY;

//     return false;
//   }

//   function onMouseMove(e) {
//     if (!dragObject.elem) return;

//     if (!dragObject.avatar) {
//       var moveX = e.pageX - dragObject.downX;
//       var moveY = e.pageY - dragObject.downY;


//       if (Math.abs(moveX) < 3 && Math.abs(moveY) < 3) {
//         return;
//       }

//       if (!dragObject.avatar) { 
//         dragObject = {};
//         return;
//       }

//       var coords = getCoords(dragObject.avatar);
//       dragObject.shiftX = dragObject.downX - coords.left;
//       dragObject.shiftY = dragObject.downY - coords.top;

//       startDrag(e);
//     }

//     dragObject.avatar.style.left = e.pageX - dragObject.shiftX + 'px';
//     dragObject.avatar.style.top = e.pageY - dragObject.shiftY + 'px';

//     return false;
//   }

//   function onMouseUp(e) {
//     if (dragObject.avatar) { // если перенос идет
//       finishDrag(e);
//     }

//     dragObject = {};
//   }

//   function finishDrag(e) {
//     var dropElem = findDroppable(e);

//     if (!dropElem) {
//       self.onDragCancel(dragObject);
//     } else {
//       self.onDragEnd(dragObject, dropElem);
//     }
//   }

//   function createAvatar(e) {

//     var avatar = dragObject.elem;
//     var old = {
//       parent: avatar.parentNode,
//       nextSibling: avatar.nextSibling,
//       position: avatar.position || '',
//       left: avatar.left || '',
//       top: avatar.top || '',
//       zIndex: avatar.zIndex || ''
//     };

//     avatar.rollback = function() {
//       old.parent.insertBefore(avatar, old.nextSibling);
//       avatar.style.position = old.position;
//       avatar.style.left = old.left;
//       avatar.style.top = old.top;
//       avatar.style.zIndex = old.zIndex
//     };

//     return avatar;
//   }

//   function startDrag(e) {
//     var avatar = dragObject.avatar;

//     document.body.appendChild(avatar);
//     avatar.style.zIndex = 9999;
//     avatar.style.position = 'absolute';
//   }

//   function findDroppable(event) {
//     dragObject.avatar.hidden = true;

//     var elem = document.elementFromPoint(event.clientX, event.clientY);

//     dragObject.avatar.hidden = false;

//     if (elem == null) {
//       return null;
//     }

//     return elem.closest('.droppable');
//   }

//   document.onmousemove = onMouseMove;
//   document.onmouseup = onMouseUp;
//   document.onmousedown = onMouseDown;

//   this.onDragEnd = function(dragObject, dropElem) {};
//   this.onDragCancel = function(dragObject) {};

// };

{$rects}

{$paths}

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
<?php \yii\widgets\Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
