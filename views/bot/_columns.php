<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function($data){
            $html = $data->name;

            if($data->using_as_template == 1){
                $html .= ' <i class="fa fa-clone fa-lg text-warning"></i>';
            }
            return $html;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'company_id',
        'value' => 'company.name',
        'filterType' => \kartik\select2\Select2::className(),
        'filterWidgetOptions' => [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\Company::find()->all(), 'id', 'name'),
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Выберите компани'],
        ],
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'type',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'status',
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'description',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'using_as_template',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'company_id',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'template' => '{view} {as-template} {copy} {update}{delete} ',
        'buttons' => [
            'copy' => function($url, $model) {
                return Html::a('<i class="fa fa-clone text-success" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Копировать',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите скопировать запись?'
                ]);
            },
            'as-template' => function($url, $model) {
                if(Yii::$app->user->identity->isSuperAdmin()){
                    return Html::a('<i class="fa fa-sticky-note text-info" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Пометить как шаблон',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Вы уверены?',
                        'data-confirm-message'=>'Вы действительно хотите пометить запись как шаблон?'
                    ]);
                }
                return '';
            },
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Изменить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ])."&nbsp;";
            },
//            'view-old' => function ($url, $model) {
//                return Html::a('<i class="fa fa-eye text-info" style="font-size: 16px;"></i>', $url, [
//                        'data-pjax'=>0, 'title'=>'Просмотр',
//                    ])."&nbsp;";
//            },
            'view' => function ($url, $model) {
                return Html::a('<i class="fa fa-magic text-warning" style="font-size: 16px;"></i>', $url, [
                        'data-pjax'=>0, 'title'=>'Просмотр (новый дизайн)',
                    ])."&nbsp;";
            },
        ],
    ],

];   