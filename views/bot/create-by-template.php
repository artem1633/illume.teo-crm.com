<?php

/**
 * @var \app\models\forms\BotByTemplateForm $model
 */

?>

<div class="row">
    <div class="col-md-12">
        <?php $form = \yii\widgets\ActiveForm::begin() ?>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'templateId')->dropDownList(
                        \yii\helpers\ArrayHelper::map(\app\models\Bot::find()->ignoreCompany()->where(['using_as_template' => 1])->all(), 'id', 'name')
                    ) ?>
                </div>
            </div>
        <?php \yii\widgets\ActiveForm::end() ?>
    </div>
</div>
