<?php
use yii\widgets\ActiveForm;

?>

<div class="bot-form">
    <?php $form = ActiveForm::begin(['id' => 'submit-form']); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'message')->textarea() ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
