<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BotSetting */

?>
<div class="bot-setting-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
