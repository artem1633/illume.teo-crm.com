<?php

/**
 * @var \app\models\BotSetting $model
 */


?>

<div class="row">
    <div class="col-md-12">
            <textarea id="js-code" cols="30" rows="10" style="width: 100%;" disabled=""><script src='https://app.illumeinc.ru/js/chat.js'></script>
            <script>
                ChatService.init({
                    id: <?=$model->id?>
                });
            </script></textarea>
        <button id="copy-js-code" class="btn btn-success btn-block" style="margin-top: 10px;">Скопировать</button>
    </div>
</div>

<?php

$script = <<< JS

$("#copy-js-code").click(function(){
    var elem = document.getElementById('js-code');
    console.log(elem);
    copyToClipboard(elem);
    $(this).text('Скопировано');
    $(this).removeClass('btn-success');
    $(this).addClass('btn-info');
});

function copyToClipboard(elem) {
      // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
          succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>