<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BotSetting */
/* @var $form yii\widgets\ActiveForm */

switch ($model->type)
{
    case \app\models\BotSetting::TYPE_VK:
        $vkVisible = true;
        $telVisible = false;
        $siteVisible = false;
        $tokenVisible = true;
        break;
    case \app\models\BotSetting::TYPE_TELEGRAM:
        $vkVisible = false;
        $telVisible = true;
        $siteVisible = false;
        $tokenVisible = true;
        break;
    case \app\models\BotSetting::TYPE_SITE:
        $vkVisible = false;
        $telVisible = false;
        $siteVisible = true;
        $tokenVisible = false;
        break;
    default:
        $vkVisible = true;
        $telVisible = false;
        $siteVisible = false;
        $tokenVisible = true;
        break;
}
//$vkSettingVisible = $model->type == \app\models\BotSetting::TYPE_TELEGRAM ? 'style="display: none;"' : '';

?>

    <div class="bot-setting-form">

        <?php $form = ActiveForm::begin(['id' => 'submit-form']); ?>

        <?= $form->field($model, 'bot_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Bot::find()->all(), 'id', 'name')) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'type')->dropDownList(\app\models\BotSetting::getTypes()) ?>

        <div class="vk-setting-wrapper" <?=$vkVisible ? '' : 'style="display: none;"'?>>
            <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="token-wrapper" <?=$tokenVisible ? '' : 'style="display: none;"' ?>>
            <?= $form->field($model, 'token')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="vk-setting-wrapper" <?=$vkVisible ? '' : 'style="display: none;"'?>>
            <?= $form->field($model, 'additional_setting')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'group_id')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'time_code')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="site-setting-wrapper" <?=$siteVisible ? '' : 'style="display: none;"'?>>
            <?= $form->field($model, 'chat_header')->textInput(['maxlength' =>  true]) ?>

            <?= $form->field($model, 'color')->widget(\kartik\color\ColorInput::className(), [
                'options' => ['placeholder' => 'Выберите цвет ...'],
            ]) ?>
        </div>


        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php

$script = <<< JS
    $('#botsetting-type').change(function(){
        if($(this).val() == 1){
            $('.vk-setting-wrapper').slideUp();
            $('.token-wrapper').slideDown();
            $('.site-setting-wrapper').slideUp();
        } else if($(this).val() == 0) {
            $('.vk-setting-wrapper').slideDown();
            $('.token-wrapper').slideDown();
            $('.site-setting-wrapper').slideUp();
        } else if($(this).val() == 2){
            $('.vk-setting-wrapper').slideUp();
            $('.token-wrapper').slideUp();
            $('.site-setting-wrapper').slideDown();
        }
    });

    $('#create-button').click(function(){
        $(this).text("Подключение ...");
        $('#submit-form').submit();
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>