<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyTelegram */
?>
<div class="company-telegram-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'company_id',
            'telegram_id',
        ],
    ]) ?>

</div>
