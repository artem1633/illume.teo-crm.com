<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $chatHistory \app\models\ChatHistory[] */
?>
<div class="customer-view">

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Чат</h4>
                </div>
                <div class="panel-body" style="height: 60vh; overflow-y: scroll;">
                    <?php \yii\widgets\Pjax::begin(['id' => 'chat-pjax-container', 'enablePushState' => false]) ?>
                    <?php foreach ($chatHistory as $message): ?>
                        <?php if($message->sender == \app\models\ChatHistory::SENDER_CUSTOMER): ?>
                            <div class="alert alert-success">
                                <h5><?=$model->fio?></h5>
                                <p><?=$message->text?></p>
                            </div>
                        <?php else: ?>
                            <div class="alert alert-warning">
                                <h5>Бот</h5>
                                <p><p><?=$message->text?></p></p>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php \yii\widgets\Pjax::end() ?>
                </div>
                <div class="panel-footer">
                    <?php \yii\widgets\Pjax::begin(['id' => 'form-pjax-container', 'enablePushState' => false]) ?>
                    <?= $this->render('chat-form',[
                        'model' => $model,
                    ]) ?>
                    <?php \yii\widgets\Pjax::end() ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title"><?=$model->fio?></h4>
                </div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'attribute' => 'botSetting.name',
                                'label' => 'Источник',
                            ],
                            'id',
                            'last_name',
                            'name',
                            'patronymic',
                            'email:email',
                            'phone',
                            [
                                'label' => 'Статус',
                                'attribute' => 'status.name',
                            ],
                            [
                                'attribute' => 'unknown_reaction',
                                'value' => function($data){
                                    return \app\widgets\BooleanValue::widget(['value' => $data->unknown_reaction]);
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'new_message',
                                'value' => function($data){
                                    return \app\widgets\BooleanValue::widget(['value' => $data->new_message]);
                                },
                                'format' => 'raw'
                            ],
                            'last_message_id',
                            'account_id',
                            [
                                'attribute' => 'last_message_datetime',
                                'format' => ['date', 'php: d M Y H:i:s']
                            ],
                            'bot_id',
                            [
                                'attribute' => 'company.name',
                                'label' => 'Компания',
                            ],
                            [
                                'attribute' => 'created_at',
                                'format' => ['date', 'php: d M Y H:i:s']
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>
