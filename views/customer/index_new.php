<?php

use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\ChatHistory;
use app\models\BotSetting;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $activeCustomer \app\models\Customer|null */

$this->title = "Покупатели";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

if($activeCustomer != null){
    $messages = \app\models\ChatHistory::find()->where(['customer_id' => $activeCustomer->id])->all();
    $botSetting = $activeCustomer->botSetting;
} else {
    $messages = [];
    $botSetting = null;
}

if(Yii::$app->user->identity->isSuperAdmin()){
    $bots = \app\models\Bot::find()->all();
    $customerStatuses = \app\models\CustomerStatus::find()->all();
    $botSettings = \app\models\BotSetting::find()->all();
} else {
    $bots = \app\models\Bot::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all();
    $customerStatuses = \app\models\CustomerStatus::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all();
    $botSettings = \app\models\BotSetting::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all();
}

?>
<?php \yii\widgets\Pjax::begin(['id' => 'pjax-chat-container']) ?>
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Фильтры</h4>
        <div class="panel-heading-btn" style="margin-top: -20px;">
            <a href="#" class="btn btn-icon btn-circle btn-xs btn-warning" data-click="panel-collapse"><i class="fa fa-arrow-down"></i></a>
        </div>
    </div>
    <div class="panel-body" style="display: none;">
        <?php $form = \yii\widgets\ActiveForm::begin(['id' => 'search-form', 'method' => 'GET']) ?>
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($searchModel, 'universal')->textInput()->label('Общий поиск') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($searchModel, 'status_id')->dropDownList(\yii\helpers\ArrayHelper::map($customerStatuses, 'id', 'name'), ['prompt' => 'Выберите']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($searchModel, 'bot_id')->dropDownList(\yii\helpers\ArrayHelper::map($bots, 'id', 'name'), ['prompt' => 'Выберите']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($searchModel, 'bot_setting_id')->dropDownList(\yii\helpers\ArrayHelper::map($botSettings, 'id', 'name'), ['prompt' => 'Выберите']) ?>
            </div>
        </div>
        <?php \yii\widgets\ActiveForm::end() ?>
    </div>
</div>
<div class="chat-main">
    <div class="chat-left">
        <ul class="chat-list">
            <?php foreach($dataProvider->models as $model): ?>
                <?php /** @var \app\models\Customer $model */

                $lastMessage = ChatHistory::find()->where(['customer_id' => $model->id])->orderBy('id desc')->one();

                $itemClass = 'list-item';
                if($activeCustomer != null){
                    if($model->id == $activeCustomer->id){
                        $itemClass = 'list-item active';
                    }
                }

                if($model->unknown_reaction == 1){
                    $itemClass .= ' item-danger';
                }

                $fio = $model->getFio();
                $fullFio = $fio;

                if(strlen($fio) > 26){
                    $fio = iconv_substr($fio, 0, 26, 'UTF-8').'...';
                }

                ?>
                <li class="<?=$itemClass?>" data-chat-id="<?=$model->id?>">
                    <a href="<?=Url::toRoute(['index', 'CustomerSearch[activeCustomerId]' => $model->id])?>">
                        <img src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" alt=""><h4 title="<?= $fullFio ?>"><?= $fio ?></h4>
                        <?php if($lastMessage != null): ?>
                            <span data-role="chat-last-message-time"><?= Yii::$app->formatter->asDate($lastMessage->message_send_datetime, 'php:H:i') ?></span>
                            <?php
                            $lastMessageText = $lastMessage->text;
                            if(strlen($lastMessageText) > 25){
                                $lastMessageText = iconv_substr($lastMessageText, 0, 25, 'UTF-8').'...';
                            }
                            ?>
                            <p data-role="chat-last-message-text"><?=$lastMessageText?></p>
                        <?php else: ?>
                            <p data-role="chat-last-message-text"><i>Нет сообщений </i></p>
                        <?php endif; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div id="chat-center" class="chat-center">
        <div id="chat-center-messages" class="chat-center-messages">
            <?php if($activeCustomer == null): ?>
                <p style="text-align: center; font-size: 20px; margin-top: 20%;">Выберите покупателя</p>
            <?php elseif(count($messages) == 0): ?>
                <p style="text-align: center; font-size: 20px; margin-top: 20%;">Нет сообщений</p>
            <?php else: ?>
                <?php foreach ($messages as $message): ?>
                    <?php /* @var \app\models\ChatHistory $message */ ?>
                    <?php if($message->sender == \app\models\ChatHistory::SENDER_CUSTOMER): ?>
                        <div class="chat-message chat-message-client">
                            <img src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" alt="">
                            <div class="message">
                                <?php if($message->attachment == null): ?>
                                    <p><?=$message->text?></p>
                                <?php else: ?>
                                    <p><a href="/<?=$message->attachment?>" data-pjax="0"><?=$message->text?></a></p>
                                <?php endif; ?>
                            </div>
                            <span><?= Yii::$app->formatter->asDatetime($message->message_send_datetime, 'php:H:i') ?></span>
                        </div>
                    <?php elseif($message->sender == \app\models\ChatHistory::SENDER_BOT): ?>
                        <div class="chat-message chat-message-me">
                            <div class="message">
                                <?php if($message->attachment == null): ?>
                                    <p><?=$message->text?></p>
                                <?php else: ?>
                                    <p><a href="/<?=$message->attachment?>" data-pjax="0"><?=$message->text?></a></p>
                                <?php endif; ?>
                            </div>
                            <span><?= Yii::$app->formatter->asDatetime($message->message_send_datetime, 'php:H:i') ?></span>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="anchor"></div>
        </div>
        <div class="form">
            <form action="#">
                <textarea id="chat-text"></textarea>
                <button id="chat-send-btn" class="btn btn-success"><i class="fa fa-send"></i></button>
            </form>
        </div>
    </div>
    <div class="chat-right">
        <?php if($activeCustomer != null): ?>
            <div class="img-wrapper">
                <?= Html::a('<i class="fa fa-trash" style="font-size: 22px;"></i>', ['delete', 'id' => $activeCustomer->id], ['class' => 'btn btn-danger btn-right',
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?']) ?>
                <?= Html::a('<i class="fa fa-pencil" style="font-size: 22px;"></i>', ['update', 'id' => $activeCustomer->id], ['class' => 'btn btn-primary btn-left',
                    'role'=>'modal-remote', 'title'=>'Удалить']) ?>
                <img src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" alt="">
            </div>
            <h3><?=$activeCustomer->fio?></h3>
            <p><b></b></p>
            <?= DetailView::widget([
                'model' => $activeCustomer,
                'attributes' => [
                    [
                        'attribute' => 'botSetting.name',
                        'label' => 'Источник',
                    ],
                    'email:email',
                    'phone',
                    [
                        'label' => 'Статус',
                        'attribute' => 'status.name',
                    ],
                    [
                        'attribute' => 'unknown_reaction',
                        'value' => function($data){
                            return \app\widgets\BooleanValue::widget(['value' => $data->unknown_reaction]);
                        },
                        'format' => 'raw'
                    ],
                    [
                        'attribute' => 'new_message',
                        'value' => function($data){
                            return \app\widgets\BooleanValue::widget(['value' => $data->new_message]);
                        },
                        'format' => 'raw'
                    ],
                    'last_message_id',
                    'account_id',
                    [
                        'attribute' => 'last_message_datetime',
                        'format' => ['date', 'php: d M Y H:i:s']
                    ],
                    'bot_id',
                    [
                        'attribute' => 'company.name',
                        'label' => 'Компания',
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => ['date', 'php: d M Y H:i:s']
                    ],
                ],
            ]) ?>
        <?php endif; ?>
    </div>
</div>

<?php

$sendUrl = Url::toRoute(['customer/send-message']);

if($activeCustomer != null){
    $customerId = $activeCustomer->id;
} else {
    $customerId = null;
}

if($activeCustomer != null){

    $script = <<< JS
    $('#chat-send-btn').click(function(e){
        e.preventDefault();
        var content = $('#chat-text').val();
        var url = "{$sendUrl}";
        
        if(content === ''){
            return;
        }
        
        $.ajax({
            'method': "POST",
            'url': url,
            'data': {
                'ChatHistory[customer_id]': {$customerId},
                'ChatHistory[text]': content,
            },
            'success': function(response){
                $('#chat-text').val('');
                updateLastMessage(response.customer_id, response.text, response.message_send_datetime);
                addAdminMessage(response.text, response.message_send_datetime);
                // $.pjax.reload('#pjax-chat-container');
            }
        });
    });
JS;

    $this->registerJs($script, \yii\web\View::POS_READY);
}

$userId = Yii::$app->user->getId();


if($customerId == null){
    $customerId = 'null';
}

$script = <<< JS

    var activeCustomerId = {$customerId};

    function addClientMessage(text, time, attachment)
    {
        var html = '<div class="chat-message chat-message-client">';
        html += '<img src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" alt="">';
        html += '<div class="message">';
        if(attachment != undefined){
            html += '<a href="/'+attachment+'" data-pjax="0">'+text+'</a>';
        } else {
            html += '<p>'+text+'</p>';
        }
        html += '</div>';
        html += '<span>'+time+'</span>';
        html += '</div>';
        $('.anchor').before(html);
    }

    function addAdminMessage(text, time, attachment)
    {
        var html = '<div class="chat-message chat-message-me">';
        html += '<div class="message">';
        if(attachment != undefined){
            html += '<a href="/'+attachment+'" data-pjax="0">'+text+'</a>';
        } else {
            html += '<p>'+text+'</p>';
        }
        html += '</div>';
        html += '<span>'+time+'</span>';
        html += '</div>';
        $('.anchor').before(html);
    }

    function updateLastMessage(chatId, text, time)
    {
        if(text.length > 25){
            text = text.substr(0, 25)+'...';
        }

        $("[data-chat-id='"+chatId+"'] [data-role='chat-last-message-text']").text(text);
        $("[data-chat-id='"+chatId+"'] [data-role='chat-last-message-time']").text(time);
    }

    var socket = io('https://app.illumeinc.ru:3000?from=server&userId={$userId}');

    socket.on('message', function(data){
        var data = JSON.parse(data);
        console.log(data);
        updateLastMessage(data.chatId, data.text, data.time);
        if(data.chatId == activeCustomerId){
            if(data.likeBot == 1){
                if(data.attachment == null){
                    addAdminMessage(data.text, data.time);
                } else {
                    addAdminMessage(data.text, data.time, data.attachment);
                }
            } else {
                if(data.attachment == null){
                    addClientMessage(data.text, data.time);
                } else {
                    addClientMessage(data.text, data.time, data.attachment);
                }
            }
        }
    });

    var element = document.getElementById("chat-center-messages");
    element.scrollTop = element.scrollHeight;

    // setInterval(function(){
    //     $.pjax.reload('#pjax-chat-container');
    // }, 3000);
    
    $('#search-form select').change(function(){
        $('#search-form').submit();
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>

<?php \yii\widgets\Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

