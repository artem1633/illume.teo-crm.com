<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $message = new \app\models\ChatHistory(['customer_id' => $model->id]);
$form = ActiveForm::begin(['options' => ['data-pjax' => true], 'id' => 'chat-form', 'action' => ['customer/send-message']]) ?>
<?= $form->field($message, 'text')->textarea()->label(false) ?>
<div class="hidden">
    <?= $form->field($message, 'customer_id')->hiddenInput()->label(false); ?>
</div>
<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary btn-block']); ?>
<?php ActiveForm::end() ?>
