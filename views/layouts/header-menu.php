<?php

use app\models\Customer;

$unknownMessageCount = Customer::find()->where(['unknown_reaction' => 1])->count();

if($unknownMessageCount == 0){
    $unknownMessageLabel = "Покупатели";
} else {
    $unknownMessageLabel = "Покупатели <span class='label label-danger pull-right' title='Неизвестные реакции' style='font-size: 11px; margin-top: 3px;'>{$unknownMessageCount}</span>";
}

?>

<div id="sidebar" class="sidebar">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\Menu::widget(
            [
                'encodeLabels' => false,
                'options' => ['class' => 'nav'],
                'items' => [
//                    ['label' => 'Пользователи', 'icon' => 'fa fa-user-o', 'url' => ['/user'],],
                    ['label' => 'Компании', 'icon' => 'fa fa-users', 'url' => ['/company'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Настройки системы', 'icon' => 'fa fa-users', 'url' => ['/settings'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
//                    ['label' => 'Тикеты', 'icon' => 'fa fa-envelope', 'url' => ['/ticket']],
                    ['label' => $unknownMessageLabel, 'icon' => 'fa fa-rub', 'url' => ['/customer']],
                    ['label' => 'Боты', 'icon' => 'fa fa-send-o', 'url' => ['/bot']],
//                    ['label' => 'Подключение ботов', 'icon' => 'fa fa-vk', 'url' => ['/bot-setting']],
                    ['label' => 'Отчет', 'icon' => 'fa fa-bar-chart', 'url' => ['/report']],
                    ['label' => 'Настройки', 'icon' => 'fa fa-cog', 'url' => '#', 'options' => ['class' => 'has-sub'], 'items' => [
                        ['label' => 'Интеграция', 'url' => ['/user-settings']],
                        ['label' => 'Пользователи', 'url' => ['/user']],
                        ['label' => 'Подключения', 'url' => ['/bot-setting']],
                    ]],
                    ['label' => 'Справочники', 'icon' => 'fa fa-book', 'url' => '#', 'options' => ['class' => 'has-sub'],
                        'items' => [
                            ['label' => 'Неизвестные реакции', 'url' => ['/unknown-message'],],
                            ['label' => 'Должности', 'url' => ['/position']],
                            ['label' => 'Статусы покупателей', 'url' => ['/customer-status']],
                            ['label' => 'Подписки', 'url' => ['/subscription'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                        ],
                    ],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
