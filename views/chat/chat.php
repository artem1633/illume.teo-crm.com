<?php

/**
 * @var \app\models\BotSetting $model
 */

?>

<link rel="stylesheet" href="https://app.illumeinc.ru/css/chat.css">
<div class="salbot-wrapper" style="height: 400px; bottom: -345px;">
    <div class="salebot">

        <div class="top-panel" style="background-color: <?=$model->color?>;" data-role="chat-top-panel">
            <div class="img" style="background-color: <?=\app\services\ColorManager::darkenColor($model->color, 1.5)?>;"><img src="https://app.illumeinc.ru/img/chat_logo.png" alt=""></div>
            <div class="top-text">
                <p class="name-bot"><?=$model->chat_header?></p>
<!--                <p class="text-2 subtitle">Чат от Illume</p>-->
            </div>
        </div>

        <div class="dropdown-block" style="display: block;">
            <div id="illume-message-list" class="message-list" data-color="<?=$model->color?>">
                <div class="message-item-server">
                </div>
                <div id="illume-anchor" class="illume-anchor"></div>
            </div>
            <div class="input-data">
                <textarea style="resize: none; overflow: hidden;" autocomplete="off" autofocus="" id="salebot-input" type="text" placeholder="Введите сообщение и нажмите Enter">
                </textarea>
                <div id="illume-sending-button" class="sending-button">
                    <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.39062 6.29355C8.39062 5.92993 8.76643 5.68791 9.09747 5.83834L22.9476 12.1318C23.3385 12.3094 23.3385 12.8646 22.9476 13.0422L9.09747 19.3357C8.76643 19.4861 8.39062 19.2441 8.39062 18.8805V13.7653L20.8377 12.587L8.39062 11.4087V6.29355Z" fill="<?=$model->color?>" style="fill: <?=$model->color?>;"></path>
                        <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M4.40039 6.52832C3.84271 6.52832 3.39062 6.98041 3.39062 7.53809C3.39062 8.09577 3.84271 8.54786 4.40039 8.54786H5.38086C5.93854 8.54786 6.39062 8.09577 6.39062 7.53809C6.39062 6.98041 5.93854 6.52832 5.38086 6.52832H4.40039ZM0.390625 12.5869C0.390625 12.0293 0.842714 11.5772 1.40039 11.5772H5.38086C5.93854 11.5772 6.39062 12.0293 6.39062 12.5869C6.39062 13.1446 5.93854 13.5967 5.38086 13.5967H1.40039C0.842714 13.5967 0.390625 13.1446 0.390625 12.5869ZM3.39062 17.6358C3.39062 17.0781 3.84271 16.626 4.40039 16.626H5.38086C5.93854 16.626 6.39062 17.0781 6.39062 17.6358C6.39062 18.1934 5.93854 18.6455 5.38086 18.6455H4.40039C3.84271 18.6455 3.39062 18.1934 3.39062 17.6358Z" fill="<?=$model->color?>"></path>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>
