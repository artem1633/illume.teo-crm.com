<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BotScenario */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord == false){
    $model->variants = \yii\helpers\ArrayHelper::getColumn(\app\models\BotScenarioVariant::find()->where(['bot_scenario_id' => $model->id])->all(), 'content');
//    $model->lastScenarioIds = \yii\helpers\ArrayHelper::getColumn(\app\models\BotScenarios::find()->where(['bot_scenario_to_id' => $model->id])->all(), 'bot_scenario_from_id');
    $model->nextScenarioIds = \yii\helpers\ArrayHelper::getColumn(\app\models\BotScenarios::find()->where(['bot_scenario_from_id' => $model->id])->all(), 'bot_scenario_to_id');
}

?>

    <div class="bot-scenario-form">

        <?php $form = ActiveForm::begin(['id' => 'bot-scenario-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <!--        <div class="col-md-3">-->
            <?php
            //            /echo $form->field($model, 'lastScenarioIds')->widget(\kartik\select2\Select2::className(), [
            //                'data' => \yii\helpers\ArrayHelper::map($model->getLinkableScenarios(), 'id', 'name'),
            //                'options' => [
            //                    'placeholder' => 'Введите предыдущие сценарии...',
            //                ],
            //                'pluginOptions' => [
            //                    'allowClear' => true,
            //                    'tags' => true,
            //                    'multiple' => true,
            //                ],
            //            ])
            ?>
            <!--        </div>-->
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php echo $form->field($model, 'nextScenarioIds')->widget(\kartik\select2\Select2::className(), [
                    'data' => \yii\helpers\ArrayHelper::map($model->getLinkableScenarios(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Введите следующие сценарии...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'tags' => true,
                        'multiple' => true,
                    ],
                ])
                ?>
            </div>
        </div>

        <div class="variants-wrapper" <?=$model->isNewRecord == false && $model->any_user_answer == 1 ? 'style="display: none;"' : ''?>>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'variants')->widget(\kartik\select2\Select2::className(), [
                        'options' => [
                            'placeholder' => 'Введите варианты ответа...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'tags' => true,
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'customer_status_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\CustomerStatus::find()->all(), 'id', 'name'), ['prompt' => 'Выберите статус']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'file')->fileInput() ?>
            </div>
            <div class="col-md-6">
                <?php if($model->hasFile()): ?>
                    <?php
                    $fileName = explode('/', $model->attachment_link);
                    if(count($fileName) == 2){
                        $fileName = $fileName[1];
                    } else {
                        $fileName = 'error';
                    }
                    ?>
                    <?php if($fileName != 'error'): ?>
                        <p id="file-wrapper">
                            <?php
                            $fileName = explode('.', $fileName);
                            if(count($fileName) == 2){
                                $extension = $fileName[1];
                                echo Html::a("Текущий файл ({$extension})", '/'.$model->attachment_link, ['target' => '_blank', 'style' => 'display: inline-block; margin-top: 35px;']) .
                                    ' ' . Html::a('<i class="fa fa-trash"></i>', ['bot-scenario/delete-attachment', 'id' => $model->id], ['class' => 'text-danger', 'title' => 'Удалить файл', 'style' => 'font-size: 18px;', 'data-role' => 'delete-attachment']);
                            } else {
                                echo '<p class="text-danger">Ошибка</p>';
                            }
                            ?>
                        </p>
                    <?php else: ?>
                        <p class="text-danger">Ошибка</p>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'send_in_crm')->checkbox() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'strict_search_mode')->checkbox() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'any_user_answer')->checkbox(['onchange' => 'if($(this).is(":checked")){ $(".variants-wrapper").slideUp(); } else { $(".variants-wrapper").slideDown(); };']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'no_user_answer')->checkbox(['onchange' => 'if($(this).is(":checked")){ $(".no-answer-time-wrapper").slideDown(); } else { $(".no-answer-time-wrapper").slideUp(); };']) ?>
            </div>
        </div>

        <div class="no-answer-time-wrapper" <?=$model->isNewRecord || $model->no_user_answer == 0 ? 'style="display: none;"' : ''?>>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'no_answer_time')->input('number') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'timing_message')->textarea() ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'bot_answer')->textarea(['rows' => 6]) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-inverse btn-sm btn-block' : 'btn btn-primary btn-sm btn-block']) ?>
        </div>

        <?php if($model->isNewRecord == false && $model->is_start == false): ?>
            <div class="form-group">
                <?= Html::a('<i class="fa fa-trash"></i> Удалить','#', ['class' => 'btn btn-danger btn-sm btn-block', 'data-delete' => $model->id]) ?>
            </div>
        <?php endif; ?>

        <?php ActiveForm::end(); ?>

    </div>


<?php

$script = <<< JS

$('#bot-scenario-form').unbind('submit');

$('#bot-scenario-form').submit(function(e){
    e.preventDefault();
    
    var form = $(this);
    var url = form.attr('action');
    var formData = new FormData(this);

    $.ajax({
           type: "POST",
           url: url,
           data: formData, // serializes the form's elements.
           processData: false,
            contentType: false,
           success: function(data)
           {
               $.pjax.reload(data.forceReload);
               $('.theme-panel').removeClass('active');
           }
         });
});

$('[data-delete]').click(function(e){
    e.preventDefault();
    
    var id = $(this).data('delete');
    
    $.post('/bot-scenario/delete?id='+id, {'id': id}, function(response){
        $.pjax.reload(response.forceReload);
        $('.theme-panel').removeClass('active');
    });
});

$('[data-role="delete-attachment"]').click(function(e){
    e.preventDefault();

    var url = $(this).attr('href');
    var csrfParam = yii.getCsrfParam;
    var csrfToken = yii.getCsrfToken;

    $.ajax({
        'url': url,
        method: 'POST',
        data: {
            csrfParam: csrfToken,
        },
        success: function(response){
            $('#file-wrapper').text('Файл удален');
        },
        fail: function(response){
            $('#file-wrapper').text(response.message);
        },
    });
});

JS;
$this->registerJs($script, \yii\web\View::POS_READY);

?>