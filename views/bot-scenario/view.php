<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BotScenario */
?>
<div class="bot-scenario-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'bot_id',
            'name',
            'description:ntext',
            'strict_search_mode',
            'customer_status_id',
            'repeat',
            'any_user_answer',
            'no_user_answer',
            'no_answer_time:datetime',
            'bot_answer:ntext',
            'attachment_link',
        ],
    ]) ?>

</div>
