<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Position */
?>
<div class="position-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
