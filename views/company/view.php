<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Company */
?>
<div class="company-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'city',
            'address',
            'house',
            'flat',
            'post_index',
            'subscription_id',
            'is_super_company',
            'subscription_end_datetime',
            'access',
            'last_activity_datetime',
            'balance',
            'created_at',
        ],
    ]) ?>

</div>
