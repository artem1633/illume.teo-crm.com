<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomerStatus */

?>
<div class="customer-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
