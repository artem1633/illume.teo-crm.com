<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerStatus */
?>
<div class="customer-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'color',
            'company_id',
        ],
    ]) ?>

</div>
