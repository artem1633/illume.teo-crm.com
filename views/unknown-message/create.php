<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UnknownMessage */

?>
<div class="unknown-message-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
