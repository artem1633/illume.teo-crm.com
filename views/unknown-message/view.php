<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UnknownMessage */
?>
<div class="unknown-message-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'content',
            'bot_id',
            'company_id',
            'created_at',
        ],
    ]) ?>

</div>
