<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

/**
 * Class UserSettingsController
 * @package app\controllers
 */
class UserSettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = Yii::$app->user->identity->companySetting;

        if($model->load($request->post()) && $model->save()) {
            return $this->render('index', [
                'model' => $model
            ]);
        } else {
            return $this->render('index', [
                'model' => $model
            ]);
        }
    }
}