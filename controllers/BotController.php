<?php

namespace app\controllers;

use app\models\BotScenariosDesignerPositions;
use app\models\forms\BotByTemplateForm;
use app\models\forms\CustomerAllMessageForm;
use app\models\Position;
use Yii;
use app\models\Bot;
use app\models\BotSearch;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\ChatHistory;

/**
 * BotController implements the CRUD actions for Bot model.
 */
class BotController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bot models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BotSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Bot models.
     * @return mixed
     */
    public function actionWebhook($id)
    {
        $webhoo = BotinfoController::setWebhook($id);
        var_dump($webhoo);
        exit;
    }

    /**
     * @param $id
     * @return array
     */
    public function actionSendMessageToAll($id)
    {
        $request = Yii::$app->request;
        $model = new CustomerAllMessageForm(['botId' => $id]);

        if($model->load($request->post())){
            $model->send();
            $this->redirect(['index']);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'title'=> "Отправить всем сообщения",
            'content'=>$this->renderAjax('global_message', [
                'model' => $model,
            ]),
            'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]) .
                Html::button('Отправить',['id' => 'create-button', 'class'=>'btn btn-primary', 'onclick' => 'event.preventDefault(); $(this).text("Отправка ..."); $("#submit-form").submit();'])
        ];
    }

    /**
     * Displays a single Bot model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "бот #".$id,
                'content'=>$this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionAsTemplate($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        $model->using_as_template = 1;
        $model->save(false);

        return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }

    public function actionCopy($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        $model->copy();

        return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose' => true
        ];
    }

    /**
     * Displays a single Bot model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewOld($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "бот #".$id,
                'content'=>$this->renderAjax('view_old', [
                    'model' => $this->findModel($id),
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view_old', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionChangeCoords(){
        if(isset($_POST['bot_id']) == false ||
            isset($_POST['group_id']) == false ||
            isset($_POST['offset_top']) == false ||
            isset($_POST['offset_left']) == false){
            throw new BadRequestHttpException('no params');
        }

        $model = $this->findModel($_POST['bot_id']);
        if($model == null){
            throw new BadRequestHttpException('invalid bot id');
        }

        /** @var BotScenariosDesignerPositions $position */
        $position = BotScenariosDesignerPositions::find()->where(['bot_id' => $model->id, 'group_id' => $_POST['group_id']])->one();

        if($position){
            $position->offset_top = $_POST['offset_top'];
            $position->offset_left = $_POST['offset_left'];
            $position->save(false);
        } else {
            $position = new BotScenariosDesignerPositions([
                'bot_id' => $model->id,
                'group_id' => $_POST['group_id'],
                'offset_top' => $_POST['offset_top'],
                'offset_left' => $_POST['offset_left'],
            ]);
            $position->save(false);
        }
    }

    /**
     * Creates a new Bot model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Bot();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить бота",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавить бота",
                    'content'=>'<span class="text-success">Создание бота успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Добавить бота",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionCreateByTemplate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $form = new BotByTemplateForm();

        if($form->load($request->post()) && $form->copy()){
            return [
                'forceReload' => '#crud-datatable-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title' => 'Использовать шаблон',
                'content' => $this->renderPartial('create-by-template', [
                    'model' => $form
                ]),
                'footer' => Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
    }

    /**
     * Updates an existing Bot model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить бота #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "бот #".$id,
                    'content'=>'<span class="text-success">Изменения сохранены</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                return [
                    'title'=> "Изменить бота #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Bot model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Bot model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Bot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bot::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
