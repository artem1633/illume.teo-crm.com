<?php

namespace app\controllers;

use Yii;
use app\models\BotSetting;
use app\models\ChatHistory;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ChatController
 * @package app\controllers
 */
class ChatController extends Controller
{
    /**
     * @throws NotFoundHttpException
     * @return string
     */
    public function actionIndex($id)
    {
        $setting = BotSetting::findOne($id);

        if($setting == null){
            throw new NotFoundHttpException();
        }

        header('Access-Control-Allow-Origin: *');
        return $this->renderAjax('chat', [
            'model' => $setting,
        ]);
    }

    /**
     * @param int
     */
    public function actionHistory($customer_id)
    {
        header('Access-Control-Allow-Origin: *');
        Yii::$app->response->format = Response::FORMAT_JSON;
        $history = ChatHistory::find()->where(['customer_id' => $customer_id])->all();

        return $history;
    }
}
