<?php

namespace app\controllers;

use app\models\Report;
use app\models\ReportSearch;
use Yii;
use app\models\SubscriptionSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * ReportController implements the CRUD actions for Subscription model.
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Subscription models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ReportSearch();

        $model->load(Yii::$app->request->get());

        $report = $model->searchForChart(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $model,
            'report' => $report,
        ]);
    }
}