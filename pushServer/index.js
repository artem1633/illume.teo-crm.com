var app = require('express')();
const log = require('simple-node-logger').createSimpleLogger('server.log');
const fs = require('fs');
const cors = require('cors');
const bodyParser = require('body-parser');
const $request = require('request');

// $request({
//     url: 'https://ads.asusmkd.ru/api/v1/add-incoming-call',
//     method: 'POST',
//     json: true,
//     body: {number: 'super'},
// }, function(error, response, data){
//     console.log(data);
// });

// $request.post(
//                 'https://ads.asusmkd.ru/api/v1/add-incoming-call',
//                 {phone: 'super'},
//                 function(error, response, data){
//                     console.log(data);
//                     if(clients[companyCallApiKey] != undefined){
//                         for(let i = 0; i < clients[companyCallApiKey].length; i++)
//                         {
//                             clients[companyCallApiKey][i].emit('message', body.from.number);
//                         }
//                     }
//                 }
//             );

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors({
    origin: true,
    credentials: true,
}));

var privateKey = fs.readFileSync(__dirname+'/key.pem');
var certificate = fs.readFileSync(__dirname+'/cert.pem');

var https = require('https').createServer({
    cert: certificate,
    key: privateKey
},app);

var server = https.listen(3000, function(){
    log.info('Server started');
});

const io = require('socket.io').listen(server);


var clients = [];
var admins = [];

// Сообщение для клиента
app.post('/message', function(req, res){
    var chatId = req.body.chatId;
    var text = req.body.text;
    var attachment = req.body.attachment;

    log.info('new message to '+chatId+' with text: '+text);

    if(chatId == null || text == null){
        res.send('Bad request');
    }

    if(clients[chatId] !== undefined){
        clients[chatId].emit('message', {
            'text': text,
            'attachment': attachment,
        });
        log.info('new message to '+chatId+' with text: '+text+' successfully sent');
        res.send('success');
    } else {
        log.info('new message to '+chatId+' with text: '+text+' faild. Client not found (finding by id: '+chatId+')');
        res.send('failed');
    }
});

// Сообшение для админа
app.post('/messageadmin', function(req, res){
    var adminId = req.body.adminId;
    var text = req.body.text;
    var time = req.body.time;
    var chatId = req.body.chatId;
    var likeBot = req.body.likeBot;
    var attachment = req.body.attachment;

    log.info('new message to admin '+adminId+' with text: '+text+' from '+chatId+' Like a bot mode is '+likeBot);

    if(adminId == null || text == null || time == null || chatId == null || likeBot == null){
        res.send('Bad request');
    }

    if(admins[adminId] !== undefined){
        if(likeBot === 0){
            likeBot = false;
        } else if(likeBot === 1){
            likeBot = true;
        }
        admins[adminId].emit('message', JSON.stringify({'text': text, 'time': time, 'attachment': attachment, 'chatId': chatId, 'likeBot': likeBot}));
        res.send('success');
    } else {
        log.info('Admin by id '+adminId+' not found throw message sending');
        res.send('failed');
    }
});

io.sockets.on('connection', function(socket){
    var data = socket.request;

    if(data._query['from'] == null){
        var session = data._query['session'];
        log.info('Connected user | session: '+session);
        sessionParts = session.split('-');

        if(sessionParts.length < 2){
            return;
        }

        // if(clients[sessionParts[1]] === undefined){
        sessionParts = session.split('-');
        clients[sessionParts[1]] = socket;
        // }

        log.info('Current connections count is '+Object.keys(clients).length);
    } else {
        var id = data._query['userId'];
        log.info('connected new admin with id '+id);
        admins[id] = socket;
    }

    // if(data._query['companyCallApiKey'] !== undefined)
    // {
    //     var apiKey = data._query['companyCallApiKey'].toString();
    //     console.log(apiKey);
    //     if(apiKey != '')
    //     {
    //         log.info('User company call api key: '+apiKey+'');

    //         if(clients[apiKey] !== undefined){
    //             clients[apiKey].push(socket);
    //         } else {
    //             clients[apiKey] = [];
    //             clients[apiKey].push(socket);
    //         }
    //         console.log(clients);
    //     }
    // }

});

io.sockets.on('disconnected', function(socket){
    var data = socket.request;

    var session = data._query['session'];
    sessionParts = session.split('-');

    if(sessionParts.length < 2){
        return;
    }

    if(clients[sessionParts[1]] != undefined){
        clients[sessionParts[1]] = null;
    }

    if(clients[sessionParts[1]] == null){
        log.info('Disconnected user | session: '+session);
        log.info('Current connections count is '+Object.keys(clients).length);
    }
});

// io.sockets.on('disconnect', function(socket){
//     var data = socket.request;

//     var session = data._query['session'];
//     log.info('Connected user | session: '+session);
//     sessionParts = session.split('-');

//     if(sessionParts.length < 2){
//         return;
//     }

//     if(clients[sessionParts[1]] != undefined){
//         delete clients[sessionParts[1]];
//     }

//     log.info('Disconnected user | session: '+session);
//     log.info('Current connections count is '+Object.keys(clients).length);
// });