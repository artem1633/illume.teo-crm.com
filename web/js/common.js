$('#btn-dropdown_header').click(function(){
    if($(".dropdown-menu").is(':visible')){
        $(".dropdown-menu ").hide();
    } else {
        $(".dropdown-menu ").show();
    }
});

var menuOpened = false;


$("[data-click='top-menu-toggled']").click(function(){
    if(menuOpened == false){
        $('#sidebar').css('left', 'unset');
        menuOpened = true;
    } else {
        $('#sidebar').css('left', '-220px');
        menuOpened = false;
    }
});