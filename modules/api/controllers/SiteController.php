<?php

namespace app\modules\api\controllers;

use app\components\helpers\AnswerFinder;
use app\models\Bot;
use app\models\BotScenario;
use app\models\BotSetting;
use app\models\Chat;
use app\models\ChatHistory;
use app\models\Customer;
use app\models\Company;
use app\models\User;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class SiteController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['bot-in','testpush','bot-update','send-refer','webhook','getDialog'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }




    /**
     * @throws \Exception
     * @throws \Throwable
     * @var Customer $user
     * @var BotScenario $scenario
     * @throws \yii\db\StaleObjectException
     */
    public function actionBotIn($token, $text, $chatId = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        header('Access-Control-Allow-Origin: *');
        /** @var BotSetting $botSetting */
        $botSetting = BotSetting::find()->where(['id' => $token])->one();
        /** @var Bot $bot */
        $bot = Bot::find()->where(['id' => $botSetting->bot_id])->one();
        /** @var Customer $user */
        $user = Customer::find()->where(['account_id' => $chatId, 'bot_setting_id' => $botSetting->id, 'bot_id' => $bot->id])->one();

        \Yii::warning($user, "Customer (User) found by chatId = {$chatId}, botSettingId = {$botSetting->id}, botId = {$bot->id}");

        if ($text == '/start' || !$user) {
            if($user){
                $user->delete();
                $user = false;
            }
            if (!$user) {
                $user = new Customer([
                    'name' => 'Клиент',
                    'last_name' => date('d.m.Y'),
                    'patronymic' => date('H:i:s'),
                    'new_message' => true,
                    'last_message_id' => 0,
                    'account_id' => $chatId,
                    'bot_id' => $bot->id,
                    'bot_setting_id' => $botSetting->id,
                    'company_id' => $bot->company_id
                ]);

                if (!$user->save(false)){
                    $a = serialize($user->errors);
                    // self::sendTelMessage('247187885', "Ошибка создания {$a}", $botSetting);
                    return true;
                }
                $scenario = BotScenario::find()
                    ->where(['bot_id' => $bot->id])
                    ->andWhere(['is_start' => 1])
                    ->one();
                if ($scenario->customer_status_id) {
                    $user->status_id = $scenario->customer_status_id;
                }
                self::newStep($user, $bot,$text,0,0);
                $user->last_message_id = $scenario->id;
                $user->save(false);
//                self::sendTelMessage($user->account_id, $scenario->bot_answer, $botSetting);
                self::newStep($user, $bot,$scenario->bot_answer,0,1);

                return [
                    'chatId' => $user->account_id,
                    'text' => $scenario->bot_answer
                ];
            }

        }
        $user->unknown_reaction = false;


        $scenario = AnswerFinder::find($user, $bot, $botSetting, $text);

        if(is_bool($scenario) == false){
            if($scenario->hasFile()){
                $chat = new ChatHistory([
                    'text' => 'Документ',
                    'customer_id' => $user->id,
                    'bot_id' => $bot->id,
                    'attachment' => $scenario->attachment_link,
                    'sender' => 1,
                ]);
                $chat->save(false);
                return [
                    'text' => $scenario->bot_answer,
                    'attachment' => 'https://app.illumeinc.ru/'.$scenario->attachment_link,
                ];
            } else {
                return [
                    'text' => $scenario->bot_answer,
                ];
            }
        }
        return [];
    }

    public static function sendAdminMessage($chatHistory, $text, $likeBot = false)
    {
        $ch = curl_init('https://app.illumeinc.ru:3000/messageadmin');

        $time = date('H:i', strtotime($chatHistory->message_send_datetime));

        $adminId = 1;

        $customer = Customer::findOne($chatHistory->customer_id);

        if($customer){
            $company = Company::findOne($customer->company_id);
            if($company){
                $user = User::find()->where(['company_id' => $company->id])->one();
                if($user){
                    $adminId = $user->id;
                }
            }
        }


        if($likeBot){
            $post = "adminId={$adminId}&text={$text}&time={$time}&chatId={$chatHistory->customer_id}&likeBot=1";
        } else {
            $post = "adminId={$adminId}&text={$text}&time={$time}&chatId={$chatHistory->customer_id}&likeBot=0";
        }

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        \Yii::warning($server_output, 'Admin message sending result');

        curl_close($ch);

        return $server_output;
    }

    public static function sendAdminAttachmentMessage($chatHistory, $attachment, $likeBot = false)
    {
        $ch = curl_init('https://app.illumeinc.ru:3000/messageadmin');

        $time = date('H:i', strtotime($chatHistory->message_send_datetime));

        if($likeBot){
            $post = "adminId=1&text=Документ&attachment={$attachment}&time={$time}&chatId={$chatHistory->customer_id}&likeBot=1";
        } else {
            $post = "adminId=1&text=Документ&attachment={$attachment}&time={$time}&chatId={$chatHistory->customer_id}&likeBot=0";
        }

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close ($ch);

        return $server_output;
    }

    /**
     * @var Bot $bot
     * @var Customer $user
     * @var string $text
     */
    public function getDialog ($user, $bot, $text)//проверяем диалоги на не прочитанные сообщения
    {

    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var Customer $user
     * @var Bot $bot
     * @var BotScenario $scenario
     * @var string $text
     * @return boolean
     */
    public static function newStep($user, $bot, $text, $status, $sender = 0)
    {
        $chat = new ChatHistory([
            'customer_id' => $user->id,
            'bot_id' => $bot->id,
            'text' => $text,
            'unknown_reaction' => $status,
            'sender' => $sender,
        ]);

        $chat->save(false);

        return $chat;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $post
     * @var integer $lang
     * @return boolean
     */
    public static function getWord($post, $lang, $user)
    {
        $text = Words::find()->where(['position' => $post, 'langs_id' => $lang])->one();
//        return self::handleModel($text->value, $user);
        $alert = Settings::find()->where(['key' => 'alert_to_admin'])->one()->value;
        $info = '';
        if ($alert == '1') {
            $info .= " {$post} - ";
        }
        $info .= str_replace('\r\n', "\r\n", self::handleModel($text->value, $user));
        return $info;
    }
    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $post
     * @var integer $lang
     * @return boolean
     */
    public function actionWord()
    {
        $user = JobTable::findOne(37);
        $text = Words::find()->where(['position' => 75, 'langs_id' => 1])->one();
        return self::handleModel($text->value, $user);
    }




    /**
     * Обрабатывают строку по модели
     * @param string $text
     * @param \yii\base\Model|\yii\base\Model[] $model
     * @return string
     */
    public static function handleModel($text, $model)
    {
        $output = $text;

        $className = lcfirst(\yii\helpers\StringHelper::basename(get_class($model)));
        $tags = [];

        foreach ($model->attributes as $name => $value)
        {
            $tags['{$'."{$className}->{$name}".'}'] = $value;
        }
//        var_dump($tags);
        $output = str_ireplace(array_keys($tags), array_values($tags), $output);

        return $output;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $chat_id
     * @var integer $message_id
     * @return boolean
     */
    public function setClear($chat_id, $message_id)
    {
        if ($message_id) {
            $this->getReq('editMessageReplyMarkup',[
                'chat_id'=> $chat_id,
                'message_id'=>$message_id,
                'text' => 'my text',
                'reply_markup' => null
            ]);
        }
        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var BotSetting $botSetting
     * @return boolean
     */
    public static function sendTelMessage($userId, $text, $botSetting)
    {
        $token = $botSetting->token;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $proxy_server = '209.250.237.162:18053';

        if(is_array($userId)){
            foreach ($userId as $id) {
                $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
                $url=$url.'?'.http_build_query(['chat_id' => $id, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
//                curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
                //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

                curl_setopt($curl, CURLOPT_PROXY, $proxy);

            }
        } else {
            $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
            $url=$url.'?'.http_build_query(['chat_id' => $userId, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
//            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $curl_scraped_page = curl_exec($ch);
            curl_close($ch);
        }


        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var BotSetting $botSetting
     * @return boolean
     */
    public static function sendSiteMessage($userId, $text, $botSetting)
    {
        $request_params = array(
            'message' => $text,
            'user_id' => $userId,
            'access_token' => $botSetting->token,
            'v' => '5.0'
        );

        $get_params = http_build_query($request_params);

        file_get_contents('https://api.vk.com/method/messages.send?'. $get_params);


        return true;
    }

    public static function genCode()
    {

        $length = 5;
        $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

        do {
            $string ='';
            for( $i = 0; $i < $length; $i++) {
                $string .= $chars[rand(0,strlen($chars)-1)];
            }
        } while ( UsersList::find()->where(['partner_code' => $string])->one() != null );

        return $string;
    }

    public static function setWebhook($id)
    {
        /** @var BotSetting $botSetting */
        $botSetting = BotSetting::findOne($id);

        $proxy_server = '209.250.237.162:18053';

        $result = null;

        $url = 'https://api.telegram.org/bot'.$botSetting->token.'/setWebhook?url=https://app.illumeinc.ru/api/botinfo/bot-in?token='.$botSetting->id;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        // curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
        //        curl_setopt($ch , CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $str1=strpos($result, "{");
        $str2=strpos($result, "}");
        $result=substr($result, $str1, $str2);
        //        var_dump($result);
        $result = json_decode($result, true);
    }



    public static function actionWebhookinfo($id)
    {
//        $token = '963981719:AAFD-R2r_Ui9MjW3ieZasBnKhfHYPrJ9IpI';//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';

        /** @var Bot $bot */
        $bot = Bot::findOne($id);

        $proxy_server = '209.250.237.162:18053';

        $url = 'https://api.telegram.org/bot'.$bot->token.'/getWebhookInfo';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        // curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
//        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $str1=strpos($result, "{");
        $str2=strpos($result, "}");
        $result=substr($result, $str1, $str2);
//        var_dump($result);
        $result = json_decode($result, true);

        return $result;
    }
}
