<?php

namespace app\modules\api\controllers;

use app\models\BotScenario;
use app\models\BotSetting;
use app\models\Customer;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use app\models\CompanyTelegram;
use app\models\Settings;
use app\models\Company;

/**
 * Class TelegramNotifyController
 * @package app\modules\api\controllers
 */
class TelegramNotifyController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['testpush','bot-update','send-refer','webhook','getDialog'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    /**
     * @throws \Exception
     * @throws \Throwable
     * @var Customer $user
     * @var BotScenario $scenario
     * @throws \yii\db\StaleObjectException
     */
    public function actionBotIn()
    {
        /** @var Bot $bot */

        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content,true); //декодируем апдейт json, пришедший с телеграмма



        $text = $result["message"]["text"]; //Текст сообщения
        $chat_id = $result["message"]["chat"]["id"]; //Уникальный идентификатор пользователя
        $first = $result["message"]["chat"]["first_name"]; //Уникальный идентификатор пользователя
        $username = $result["message"]["chat"]["username"]; //Уникальный идентификатор пользователя
        $last = $result["message"]["from"]["last_name"]; //Юзернейм пользователя


        if ($text == '/start') {
            self::sendTelMessage($chat_id, "Для подключения уведомлений введите код компании");
            return true;
        }

        $company = Company::find()->where(['code' => $text])->one();

        if($company == false){
            self::sendTelMessage($chat_id, 'Код компании не верный');
            return true;
        }

        $companyTelegram = CompanyTelegram::find()->where(['telegram_id' => $chat_id])->one();

        if($companyTelegram){
            self::sendTelMessage($chat_id, 'Вы уже подключены');
            return true;
        } else {
            $companyTelegram = new CompanyTelegram([
                'company_id' => $company->id,
                'telegram_id' => $chat_id,
            ]);
            $companyTelegram->save(false);

            self::sendTelMessage($chat_id, 'Вы успешно подключены');
            return true;
        }

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var BotSetting $botSetting
     * @return boolean
     */
    public static function sendTelMessage($userId, $text)
    {
        $token = Settings::findByKey('telegram_token')->value;
        // $proxy_server = Settings::findByKey('proxy_server')->value;

        if(is_array($userId)){
            foreach ($userId as $id) {
                $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
                $url=$url.'?'.http_build_query(['chat_id' => $id, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                // curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
                //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

            }
        } else {
            $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
            $url=$url.'?'.http_build_query(['chat_id' => $userId, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            // curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
//            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $curl_scraped_page = curl_exec($ch);
            curl_close($ch);
        }


        return true;
    }

}