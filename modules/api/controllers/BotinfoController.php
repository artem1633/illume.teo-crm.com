<?php

namespace app\modules\api\controllers;

use Yii;
use app\components\helpers\AnswerFinder;
use app\models\Bot;
use app\models\BotScenario;
use app\models\BotSetting;
use app\models\Chat;
use app\models\ChatHistory;
use app\models\Customer;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\helpers\ArrayHelper;

/**
 * Default controller for the `api` module
 */
class BotinfoController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['bot-in','testpush','bot-update','send-refer','webhook','getDialog'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }




    /**
     * @throws \Exception
     * @throws \Throwable
     * @var Customer $user
     * @var BotScenario $scenario
     * @throws \yii\db\StaleObjectException
     */
    public function actionBotIn($token)
    {
    	\Yii::warning('Bot-in action', 'Bot-in action');

        /** @var Bot $bot */

        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content,true); //декодируем апдейт json, пришедший с телеграмма


//        self::sendTelMessage('247187885', "Ошибка создания {$a}", $botSetting);
//        return true;
        $text = $result["message"]["text"]; //Текст сообщения
        $chat_id = $result["message"]["chat"]["id"]; //Уникальный идентификатор пользователя
        $first = $result["message"]["chat"]["first_name"]; //Уникальный идентификатор пользователя
        $username = $result["message"]["chat"]["username"]; //Уникальный идентификатор пользователя
        $last = $result["message"]["from"]["last_name"]; //Юзернейм пользователя

        // $user_push = ['247187885'];//,'541366830'

        /** @var BotSetting $botSetting */
        $botSetting = BotSetting::find()->where(['id' => $token])->one();
        /** @var Bot $bot */
        $bot = Bot::find()->where(['id' => $botSetting->bot_id])->one();
        /** @var Customer $user */
        $user = Customer::find()->where(['account_id' => $chat_id, 'bot_setting_id' => $botSetting->id, 'bot_id' => $bot->id])->one();

        if ($text == '/start' || !$user) {

            if($user){
                $user->delete();
                $user = false;
            }
            if (!$user) {
                $user = new Customer([
                    'last_name' => $last,
                    'name' => $first,
                    'patronymic' => $username,
                    'new_message' => true,
                    'last_message_id' => 0,
                    'account_id' =>$chat_id,
                    'bot_id' => $bot->id,
                    'bot_setting_id' => $botSetting->id,
                    'company_id' => $bot->company_id
                ]);

                if (!$user->save(false)){
                    $a = serialize($user->errors);
                    self::sendTelMessage('247187885', "Ошибка создания {$a}", $botSetting);
                    Yii::warning("Ошибка создания {$a}");
                    return true;
                }
                $scenario = BotScenario::find()
                    ->where(['bot_id' => $bot->id])
                    ->andWhere(['is_start' => 1])
                    ->one();
                if ($scenario->customer_status_id) {
                    $user->status_id = $scenario->customer_status_id;
                }
                self::newStep($user, $bot,$text,0,0);
                $user->last_message_id = $scenario->id;
                $user->save(false);
                $user->message->sendMessage($user->account_id, $scenario->bot_answer);
                // self::sendTelMessage($user->account_id, $scenario->bot_answer, $botSetting);
                self::newStep($user, $bot,$scenario->bot_answer,0,1);
                return true;
            }

        }
        $user->unknown_reaction = false;

        $scenario = AnswerFinder::find($user, $bot, $botSetting, $text);

        if(is_bool($scenario)){
            return true;
        } else {
            if($scenario->hasFile()){
                $chat = new ChatHistory([
                    'text' => 'Документ',
                    'customer_id' => $user->id,
                    'bot_id' => $bot->id,
                    'attachment' => $scenario->attachment_link,
                    'sender' => 1,
                ]);

                $chat->save(false);
            }
        }

        return true;
    }


    public function actionVkTest()
    {
        $customer = Customer::findOne(116);

        $result = $customer->message->sendFile('https://illume.teo-crm.com/'.'uploads/zHWmfP1K-0Zcsavqn498vmGArGYw1RM-.css');

        // var_dump($result);
    }

    /**
     * @var Bot $bot
     * @var Customer $user
     * @var string $text
     */
    public function getDialog ($user, $bot, $text)//проверяем диалоги на не прочитанные сообщения
    {

    }

    public function actionTimingMessage()
    {
        $timingMessageScenariosPks = ArrayHelper::getColumn(BotScenario::find()->where(['no_user_answer' => 1])->all(), 'id');

        $customers = Customer::find()->where(['last_message_id' => $timingMessageScenariosPks])->andWhere(['timing_message_sent' => 0])->all();

        // var_dump($customers);
        // exit;

        foreach ($customers as $customer) {
            $scenario = BotScenario::findOne($customer->last_message_id);
            $start = strtotime(date('Y-m-d H:i', strtotime($customer->last_message_datetime)));
            $minutes = $scenario->no_answer_time * 60;
            $end = $start + $minutes;

            echo $customer->name.' found<br>';

            if(time() > $end){
                echo 'sent<br>';
                $customer->message->sendMessage($customer->account_id, $scenario->timing_message);
                $msg = new ChatHistory([
                    'customer_id' => $customer->id,
                    'bot_id' => $scenario->bot_id,
                    'text' => $scenario->timing_message,
                    'sender' => ChatHistory::SENDER_BOT,
                ]);
                $msg->save(false);
                $customer->timing_message_sent = 1;
                $customer->save(false);
                // if($scenario->hasFile()){
                // $customer->message->sendFile('https://illume.teo-crm.com/'.$scenario->attachment_link);
                // }
            } else {
                $start = date('Y-m-d H:i:s', $start);
                $end = date('Y-m-d H:i:s', $end);
                echo "start: {$start} | end: {$end}<br>";
                echo 'not sent<br>';
            }

            echo '<br>';
        }
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var Customer $user
     * @var Bot $bot
     * @var BotScenario $scenario
     * @var string $text
     * @return boolean
     */
    public static function newStep($user, $bot, $text, $status, $sender = 0)
    {
        try {
            $userModel = Customer::find()->where(['id' => $user->id])->one();

            if($userModel){
                $chat = new ChatHistory([
                    'customer_id' => $user->id,
                    'bot_id' => $bot->id,
                    'text' => $text,
                    'unknown_reaction' => $status,
                    'sender' => $sender,
                ]);

                $chat->save(false);
            } else {
                return null;
            }
        } catch (\Exception $e){
            \Yii::warning($e->getMessage(), 'exception while newStep func');
            return null;
        }

        return $chat;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $post
     * @var integer $lang
     * @return boolean
     */
    public static function getWord($post, $lang, $user)
    {
        $text = Words::find()->where(['position' => $post, 'langs_id' => $lang])->one();
//        return self::handleModel($text->value, $user);
        $alert = Settings::find()->where(['key' => 'alert_to_admin'])->one()->value;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $info = '';
        if ($alert == '1') {
            $info .= " {$post} - ";
        }
        $info .= str_replace('\r\n', "\r\n", self::handleModel($text->value, $user));
        return $info;
    }
    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $post
     * @var integer $lang
     * @return boolean
     */
    public function actionWord()
    {
        $user = JobTable::findOne(37);
        $text = Words::find()->where(['position' => 75, 'langs_id' => 1])->one();
        return self::handleModel($text->value, $user);
    }




    /**
     * Обрабатывают строку по модели
     * @param string $text
     * @param \yii\base\Model|\yii\base\Model[] $model
     * @return string
     */
    public static function handleModel($text, $model)
    {
        $output = $text;

        $className = lcfirst(\yii\helpers\StringHelper::basename(get_class($model)));
        $tags = [];

        foreach ($model->attributes as $name => $value)
        {
            $tags['{$'."{$className}->{$name}".'}'] = $value;
        }
//        var_dump($tags);
        $output = str_ireplace(array_keys($tags), array_values($tags), $output);

        return $output;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $chat_id
     * @var integer $message_id
     * @return boolean
     */
    public function setClear($chat_id, $message_id)
    {
        if ($message_id) {
            $this->getReq('editMessageReplyMarkup',[
                'chat_id'=> $chat_id,
                'message_id'=>$message_id,
                'text' => 'my text',
                'reply_markup' => null
            ]);
        }
        return true;
    }


    public function actionTelTest()
    {
        self::sendTelMessage('300640816', 'Gi', BotSetting::find()->where(['id' => 19])->one());
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var BotSetting $botSetting
     * @return boolean
     */
    public static function sendTelMessage($userId, $text, $botSetting)
    {
        $token = $botSetting->token;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $proxy_server = '129.146.181.251:3128';

        if(is_array($userId)){
            foreach ($userId as $id) {
                $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
                $url=$url.'?'.http_build_query(['chat_id' => $id, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
                //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

                curl_setopt($curl, CURLOPT_PROXY, $proxy);

            }
        } else {
            $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
            $url=$url.'?'.http_build_query(['chat_id' => $userId, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
//            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $curl_scraped_page = curl_exec($ch);
            curl_close($ch);
        }

        $a = serialize($curl_scraped_page);
        Yii::warning("Ошибка создания {$a}");
        return true;
    }



    public static function genCode()
    {

        $length = 5;
        $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

        do {
            $string ='';
            for( $i = 0; $i < $length; $i++) {
                $string .= $chars[rand(0,strlen($chars)-1)];
            }
        } while ( UsersList::find()->where(['partner_code' => $string])->one() != null );

        return $string;
    }

    public static function setWebhook($id)
    {
        /** @var BotSetting $botSetting */
        $botSetting = BotSetting::findOne($id);

        // $proxy_server = '188.166.160.106:80';

        $result = null;

        $url = 'https://api.telegram.org/bot'.$botSetting->token.'/setWebhook?url=https://app.illumeinc.ru/api/botinfo/bot-in?token='.$botSetting->id;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        // curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
        //        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $str1=strpos($result, "{");
        $str2=strpos($result, "}");
        $result=substr($result, $str1, $str2);
        //        var_dump($result);
        $result = json_decode($result, true);

        return $result;
    }



    public static function actionWebhookinfo($id)
    {
//        $token = '963981719:AAFD-R2r_Ui9MjW3ieZasBnKhfHYPrJ9IpI';//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';

        /** @var Bot $bot */
        $bot = Bot::findOne($id);

        $proxy_server = '209.250.237.162:18053';

        $url = 'https://api.telegram.org/bot'.$bot->token.'/getWebhookInfo';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
//        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $str1=strpos($result, "{");
        $str2=strpos($result, "}");
        $result=substr($result, $str1, $str2);
//        var_dump($result);
        $result = json_decode($result, true);

        return $result;
    }
}
